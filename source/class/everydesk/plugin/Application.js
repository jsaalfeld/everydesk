/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/background/*)
 * @asset(everydesk/icons/16x16/*)
 * @asset(everydesk/icons/32x32/*)
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.Application', {
    extend: qx.ui.window.Window,
    type: 'abstract',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        state: null,

        init: function() {
            var me = this;
            me.debug('Initialize Application');
        },

        getState: function() {
            var me = this;
            me.state = {
                width: me.getWidth(),
                height: me.getHeight(),
                layoutProperties: me.getLayoutProperties()
            };
            return me.state;
        },

        open: function() {},
        getFileTypes: function() {
            if (this.__fileTypes) {
                return this.__fileTypes;
            }
        },
        centerWindow: function() {
            var width = window.innerWidth;
            var appWidth = this.getWidth();
            var height = window.innerHeight;
            var appHeight = this.getHeight();
            var top = height - appHeight;
            if (top > 0) {
                top = Math.floor(top/2);
            }
            else {
                top = 0;
            }
            var left = width - appWidth;
            if (left > 0) {
                left = Math.floor(left/2);
            }
            else {
                left = 0;
            }
            this.moveTo(left, top);
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);

        me.setContentPadding(0, 0, 0, 0);

        me.setWidth(300);
        me.setHeight(300);
        me.center();
    }
});
