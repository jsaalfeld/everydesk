/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.settings.Settings', {
    extend: everydesk.plugin.Application,

    members: {
        __list: null,
        __page: null,
        __pages: [],

        init: function() {

            var me = this;
            me.setCaption('Settings');
            me.setContentPadding(0);
            var mainLayout = new qx.ui.layout.VBox();
            var splitContainer = new qx.ui.splitpane.Pane('horizontal');
            me.setLayout(mainLayout);
            me.__list = new qx.ui.form.List();
            me.__list.addListener('changeSelection', function(evt) {
                me.__page.removeAll();
                var ndx = 0;
                for (var i = 0; i < me.__pages.length; i++) {
                    if (me.__pages[i].name === evt.getData()[0].getLabel()) {
                        ndx = i;
                        break;
                    }
                }
                me.__page.add(me.__pages[ndx].widget, {flex: 1});
            });
            me.__page = new qx.ui.container.Composite();
            var containerLayout = new qx.ui.layout.VBox();
            me.__page.setLayout(containerLayout);
            splitContainer.add(me.__list, 1);
            splitContainer.add(me.__page, 2);
            me.add(splitContainer, {flex: 1});
            var apps = everydesk.base.system.StarterManager.getInstance().getApps();
            this.__loadApps(apps);
        },

        __buildSettings: function(apps) {
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'actionend', {
                    type: 'plugin',
                    icon: 'everydesk/icons/16x16/application.png'
                }
            ));
            var mixinClass = qx.Mixin.getByName('everydesk.plugin.MSettings');
            for (var i = 0; i < apps.length; i++) {
                var appClass = qx.Class.getByName(apps[i].endpoint);
                if (qx.Class.hasMixin(appClass, mixinClass)) {
                    var ndx = apps[i].endpoint.lastIndexOf('.');
                    var settingsName = apps[i].endpoint.substring(0, ndx);
                    var clazz = qx.Class.getByName(settingsName + '.Settings');
                    if (!clazz) {
                        console.log('No settings found for ' + settingsName + 'Settings');
                        // show error
                        return;
                    }
                    var settings = new clazz();
                    this.__addUI(apps[i].name, settings.getUI());
                }
            }
            var first = this.__list.getChildren()[0];
            this.__list.setSelection([first]);
        },

        __addUI: function(name, widget) {
            var item = new qx.ui.form.ListItem(name);
            this.__list.add(item);
            this.__pages.push({name: name, widget: widget});
        },

        __findEndpoint: function(endpoint) {
            var parts = qx.io.PartLoader.getInstance().getParts();
            // strip endpoint class
            var ndx = endpoint.lastIndexOf('.');
            endpoint = endpoint.substring(0, ndx);
            for (var key in parts) {
                if (key.indexOf(endpoint) >= 0) {
                    return key;
                }
            }
            return endpoint;
        },

        __loadApps: function(apps) {
            var me = this;
            var toLoad = [];
            for (var i = 0; i < apps.length; i++) {
                if (apps[i].type === 'app') {
                    if (!qx.Class.isDefined(apps[i].endpoint)) {
                        var endpoint = me.__findEndpoint(apps[i].endpoint);
                        toLoad.push(endpoint);
                    }
                }
            }
            if (toLoad.length > 0) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'plugin',
                        icon: 'everydesk/icons/16x16/application.png'
                    }
                ));
                var size = toLoad.length;
                qx.io.PartLoader.require(toLoad, function(state) {
                    for (var j = 0; j < state.length; j++) {
                        if (state[j] === 'complete') {
                            size--;
                        }
                    }
                    if (size === 0) {
                        me.__buildSettings(apps);
                    }
                }, me);
            }
            else {
                me.__buildSettings(apps);
            }
        }
    },

    construct: function() {
        this.base(arguments);
        this.setWidth(600);
        this.setHeight(500);
        this.init();
    }
});
