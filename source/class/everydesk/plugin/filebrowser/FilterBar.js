/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.filebrowser.FilterBar', {
    extend: qx.ui.toolbar.ToolBar,

    /*
    ***************************************************************************
     PROPERTIES
    ***************************************************************************
    */
    properties: {
    },

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __model: null,
        __filterSelect: null,
        __fileTypes: null,

        init: function() {
            var me = this;
            me.__filterSelect = new qx.ui.form.ComboBox();
            me.__filterSelect.setMargin(3, 3, 3, 3);
            var filterLabel = new qx.ui.basic.Label('Filter:');
            filterLabel.setMargin(3, 3, 3, 3);
            filterLabel.setAlignY('middle');
            me.add(filterLabel);
            me.add(me.__filterSelect, {
                width: '60%'
            });
            me.__filterSelect.addListener('changeValue', me.applyFilter, me);
        },

        setModel: function(model) {
            this.__model = model;
        },

        applyFilter: function() {
            var me = this;
            var selected = me.__filterSelect.getValue();
            var filter = '';
            if (!me.__fileTypes) {
                return;
            }
            for (var j = 0; j < me.__fileTypes.length; j++) {
                if (me.__fileTypes[j][0] === selected) {
                    filter = me.__fileTypes[j][1];
                    break;
                }
            }
            me.__model.resetHiddenRows();
            var rowArr = me.__model.getData();
            var rowLength = rowArr.length;
            var rowsToHide = [];
            for (var i = 0; i < rowLength; i++) {
                var row = me.__model.getRowData(i);
                if (row[3].getType() === 'folder') {
                    continue;
                }
                var regex = new RegExp(filter, 'gi');
                if (regex.test(row[1])) {
                    continue;
                }
                rowsToHide.push(i);
            }

            for (var k = rowsToHide.length - 1; k >= 0; k--) {
                me.__model.hideRows(rowsToHide[k], 1);
            }
        },

        setFileTypes: function(types) {
            this.__fileTypes = types;
            for (var i = 0; i < types.length; i++) {
                var item = new qx.ui.form.ListItem(types[i][0]);
                this.__filterSelect.add(item);
            }
            this.__filterSelect.setValue(
                this.__filterSelect.getChildrenContainer()
                    .getSelectables()[0].getLabel());
        },

        getCurrentFilter: function() {
            var selected = this.__filterSelect.getValue();
            for (var j = 0; j < this.__fileTypes.length; j++) {
                if (this.__fileTypes[j][0] === selected) {
                    return this.__fileTypes[j];
                }
            }
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.init();
    }
});

