/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This is the main application class of your custom application "everydesk"
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.filebrowser.FileTree', {
    extend: qx.ui.container.Composite,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */

    members: {
        __tree: null,

        init: function() {
            var me = this;
            me.setLayout(new qx.ui.layout.VBox());

            me.__tree = new qx.ui.tree.Tree();
            var root = new qx.ui.tree.TreeFolder('everydesk');
            root.setOpen(true);
            root.setUserData('path', '/fs');
            root.setUserData('name', 'fs');
            me.__tree.setRoot(root);
            // me.__tree.setHideRoot(true);
            me.add(me.__tree, {
                flex: 1
            });
        },

        getNode: function(path, parent) {
            var me = this;
            return me.__getNode(path, parent);
        },

        __getNode: function(path, node) {
            var parts = path.split('/');
            if (path === '/fs/') {
                return node;
            }

            var children = node.getChildren();
            var ndx = 2;

            for (var i = 0; i < children.length; i++) {
                var nodeParts = children[i].getUserData('path').split('/');
                if (parts[ndx] === nodeParts[ndx] && ndx === parts.length - 2) {
                    return children[i];
                }
                else if (parts[ndx] === nodeParts[ndx]) {
                    var current = children[i];
                    while (current && ndx < parts.length) {
                        ndx++;
                        var currChildren = current.getChildren();
                        for (var j = 0; j < currChildren.length; j++) {
                            var childParts = currChildren[j].getUserData('path').split('/');
                            if (childParts[ndx] === parts[ndx] &&
                                ndx === parts.length - 2) {
                                return currChildren[j];
                            }
                            else if (childParts[ndx] === parts[ndx]) {
                                current = currChildren[j];
                                break;
                            }
                        }
                    }
                }
                else {
                }
            }
            /* for (var i = 0; i < children.length; i++) {
                if (children[i].getUserData('name') === decodeURIComponent(parts[ndx]) &&
                    ndx === parts.length - 2) {
                    return children[i];
                }
                else if (children[i].getUserData('name') === decodeURIComponent(parts[ndx])) {
                    var current = children[i];
                    while (current) {
                        var currChildren = current.getChildren();
                        ndx++;
                        for (var j = 0; j < currChildren.length; j++) {
                            if (currChildren[j].getUserData('name') === decodeURIComponent(parts[ndx]) &&
                                ndx === parts.length - 2) {
                                return currChildren[j];
                            }
                            else if (currChildren[j].getUserData('name') === decodeURIComponent(parts[ndx])) {
                                current = currChildren[j];
                                break;
                            }
                        }
                    }
                }
            }
            */
        },

        getRoot: function() {
            return this.__tree.getRoot();
        },

        addNode: function(parent, node) {
            if (node.getType() === 'folder') {
                var treeNode = new qx.ui.tree.TreeFolder(node.getDisplayName());
                treeNode.setIcon('everydesk/icons/16x16/' + everydesk.plugin.filebrowser.Filebrowser.FILEICONS[node.getMimeType()]);
                treeNode.setUserData('path', node.getPath());
                treeNode.setUserData('name', node.getName());
                treeNode.setUserData('fileRef', node);
                treeNode.setDraggable(true);
                treeNode.setDroppable(true);
                treeNode.addListener('dragstart', function(evt) {
                    evt.addType('file');
                    evt.addAction('move');
                });
                treeNode.addListener('dragover', function(evt) {
                    if (!evt.supportsType('file')) {
                        evt.preventDefault();
                    }
                    evt.getCurrentTarget().setBackgroundColor('#80B4EF');
                });
                treeNode.addListener('dragleave', function(evt) {
                    if (!evt.supportsType('file')) {
                        evt.preventDefault();
                    }
                    evt.getCurrentTarget().resetBackgroundColor();
                });
                treeNode.addListener('droprequest', function(evt) {
                    evt.addData('file', [evt.getTarget().getUserData('fileRef')]);
                });
                treeNode.addListener('drop', function(evt) {
                    evt.getCurrentTarget().resetBackgroundColor();
                    var data = evt.getData('file');
                    var target = evt.getTarget().getUserData('fileRef');
                    if (target.getType() !== 'folder') {
                        return;
                    }
                    var filemanager = everydesk.base.system.FileManager.getInstance();
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].getPath() === target.getPath()) {
                            continue;
                        }
                        filemanager.mv(data[i], target.getPath() + data[i].getDisplayName());
                    }
                });
                parent.add(treeNode);
                return treeNode;
            }
        },

        getTree: function() {
            return this.__tree;
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.init();
    }
});
