/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @require(everydesk.plugin.filebrowser.FileOpen)
 * @require(everydesk.plugin.filebrowser.FileSave)
 * @require(everydesk.plugin.filebrowser.Settings)
 */
qx.Class.define('everydesk.plugin.filebrowser.Filebrowser', {
    extend: everydesk.plugin.Application,
    include: [
        everydesk.plugin.MSettings
    ],

    statics: {
        FILEICONS: {
            'owncloud': 'network_cloud.png',
            'folder': 'folder.png',
            'audio/mpeg': 'file_extension_m4v.png',
            'image/jpeg': 'file_extension_jpg.png',
            'image/png': 'file_extension_png.png',
            'application/pdf': 'file_extension_pdf.png',
            'application/epub+zip': 'file_extension_bin.png',
            'video/x-ms-asf': 'file_extension_wma.png',
            'video/mp4': 'file_extension_wma.png',
            'text/plain': 'file_extension_txt.png',
            'application/vnd.oasis.opendocument.text': 'file_extension_odt.png',
            'application/vnd.oasis.opendocument.spreadsheet': 'file_extension_ods.png',
            'application/vnd.oasis.opendocument.presentation': 'file_extension_odp.png',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'file_extension_doc.png',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'file_extension_xls.png',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'file_extension_pps.png'

        }
    },

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __tree: null,
        __list: null,
        __fileTree: null,
        __treeCurrentSelection: null,
        __treeHistory: [],
        __treeHistoryCursor: -1,
        __forwardButton: null,
        __backButton: null,
        __refreshButton: null,
        __uploadQueue: [],
        __uploadsInProgress: [],
        __uploadFolder: null,
        __maxUploads: 5,

        init: function() {
            var me = this;
            me.setCaption('Filebrowser');
            me.setContentPadding(0);
            me.__fileTree = everydesk.base.system.FileManager.getInstance().getFileSystem();
            var toolbar = new qx.ui.toolbar.ToolBar();
            toolbar.setSpacing(3);
            me.add(toolbar);
            var tbPart1 = new qx.ui.toolbar.Part();
            var tbPart2 = new qx.ui.toolbar.Part();
            me.__refreshButton = new qx.ui.toolbar.Button(
                'Refresh',
                'everydesk/icons/16x16/arrow_refresh.png');
            me.__refreshButton.addListener('execute', me.refresh, me);
            me.__refreshButton.setEnabled(false);
            me.__backButton = new qx.ui.toolbar.Button(
                'Back',
                'everydesk/icons/16x16/undo.png');
            me.__backButton.addListener('execute', me.__backFolder, me);
            me.__forwardButton = new qx.ui.toolbar.Button(
                'Forward',
                'everydesk/icons/16x16/redo.png');
            me.__forwardButton.addListener('execute', me.__forwardFolder, me);
            me.__addFolderButton = new qx.ui.toolbar.Button(
                'New Folder',
                'everydesk/icons/16x16/folder_add.png');
            me.__addFolderButton.addListener('execute', me.__addFolder, me);
            me.__uploadButton = new com.zenesis.qx.upload.UploadToolbarButton(
                'Upload',
                'everydesk/icons/16x16/upload_for_cloud.png');
            me.__uploadButton.addListener('execute', me.__upload, me);
            me.__downloadButton = new qx.ui.toolbar.Button(
                'Download',
                'everydesk/icons/16x16/download_cloud.png');
            me.__downloadButton.addListener('execute', me.__download, me);
            me.__removeButton = new qx.ui.toolbar.Button(
                'Delete',
                'everydesk/icons/16x16/delete.png');
            me.__removeButton.addListener('execute', me.__remove, me);
            me.__editButton = new qx.ui.toolbar.Button(
                'Edit',
                'everydesk/icons/16x16/edit_button.png');
            me.__editButton.addListener('execute', me.__edit, me);
            me.__addFolderButton.setEnabled(false);
            me.__removeButton.setEnabled(false);
            me.__editButton.setEnabled(false);
            tbPart1.add(me.__refreshButton);
            tbPart1.add(me.__backButton);
            tbPart1.add(me.__forwardButton);
            tbPart2.add(me.__addFolderButton);
            tbPart2.add(me.__uploadButton);
            tbPart2.add(me.__downloadButton);
            tbPart2.add(me.__removeButton);
            tbPart2.add(me.__editButton);
            toolbar.add(tbPart1);
            toolbar.add(tbPart2);
            tbPart1.setShow('icon');
            tbPart2.setShow('icon');
            me.__backButton.setEnabled(false);
            me.__forwardButton.setEnabled(false);
            var mainLayout = new qx.ui.layout.VBox();
            me.setLayout(mainLayout);
            var splitPane = new qx.ui.splitpane.Pane('horizontal');
            me.setLayout(mainLayout);
            me.add(splitPane, {flex: 1});
            me.__uploader = new com.zenesis.qx.upload.UploadMgr(me.__uploadButton, '/');
            me.__uploader.__uploadHandler = new everydesk.plugin.filebrowser.FileUploadHandler(me.__uploader);
            me.__tree = new everydesk.plugin.filebrowser.FileTree();
            me.__list = new everydesk.plugin.filebrowser.FileList();
            me.__tree.getTree().addListener('click', function() {
                var selection = me.__tree.getTree().getSelection();
                if (!selection[0] ||
                    selection[0] === me.__treeCurrentSelection) {
                    return;
                }
                var node = selection[0].getUserData('fileRef');
                if (!node) {
                    node = me.__fileTree;
                    me.__list.setFiles(node.children, node);
                    me.__list.getList().resetSelection();
                    me.__refreshButton.setEnabled(false);
                    me.__addFolderButton.setEnabled(false);
                    me.__removeButton.setEnabled(false);
                }
                else {
                    me.__list.setFiles(node.getChildren(), node);
                    me.__list.getList().resetSelection();
                    me.__refreshButton.setEnabled(true);
                    me.__addFolderButton.setEnabled(true);
                    me.__removeButton.setEnabled(true);
                }
                me.__editButton.setEnabled(false);
                selection[0].setOpen(true);
                if (me.__treeCurrentSelection &&
                    selection[0] !== me.__treeCurrentSelection &&
                    !me.__treeCurrentSelection.hasChildren()) {
                    me.__treeCurrentSelection.setOpen(false);
                }
                me.__treeCurrentSelection = selection[0];
                if (me.__treeHistoryCursor < me.__treeHistory.length - 1) {
                    me.__treeHistory.splice(me.__treeHistoryCursor + 1);
                    me.__forwardButton.setEnabled(false);
                }
                me.__treeHistory.push(selection[0]);
                me.__treeHistoryCursor++;
                if (me.__treeHistory.length > 1) {
                    me.__backButton.setEnabled(true);
                }
            });
            me.__list.getList().getSelectionModel().addListener('changeSelection', function(evt) {
                var selectionModel = evt.getCurrentTarget();
                if (selectionModel.getSelectedCount() === 1) {
                    me.__editButton.setEnabled(true);
                }
                else {
                    me.__editButton.setEnabled(false);
                }
            });
            me.__list.getList().addListener('dblclick', function(evt) {
                evt.stopPropagation();
                var selectionModel = evt.getCurrentTarget().getSelectionModel();
                if (selectionModel.getSelectedCount() > 0) {
                    var ndx = selectionModel.getSelectedRanges()[0].maxIndex;
                    var node = me.__tree.getNode(
                        me.__list.getModel().getValue(3, ndx).getPath(),
                        me.__tree.getRoot());
                    var file = me.__list.getModel().getValue(3, ndx);
                    node.setOpen(true);
                    if (node !== me.__treeCurrentSelection) {
                        if (!me.__treeCurrentSelection.hasChildren()) {
                            me.__treeCurrentSelection.setOpen(false);
                        }
                        me.__treeCurrentSelection = node;
                        me.__tree.getTree().setSelection([node]);
                    }
                    if (file && file.getType() === 'file') {
                        var msg = new qx.event.message.Message('openfile', file);
                        msg.setSender({
                            source: 'filebrowser',
                            cause: 'useraction'
                        });
                        qx.event.message.Bus.dispatch(msg);
                    }
                    else if (node) {
                        me.__list.setFiles(
                            node.getUserData('fileRef').getChildren(),
                            node.getUserData('fileRef'));
                        me.__list.getList().resetSelection();
                        if (me.__treeHistoryCursor < me.__treeHistory.length - 1) {
                            me.__treeHistory.splice(me.__treeHistoryCursor + 1);
                            me.__forwardButton.setEnabled(false);
                        }
                        me.__treeHistory.push(node);
                        me.__treeHistoryCursor++;
                        if (me.__treeHistory.length > 1) {
                            me.__backButton.setEnabled(true);
                        }
                    }
                }
            }, me, true);
            splitPane.add(me.__tree, 1);
            splitPane.add(me.__list, 2);
            me.loadFileSystems();
        },

        __backFolder: function() {
            var me = this;
            if (me.__treeHistoryCursor > 0 &&
                me.__treeHistory.length > 1) {
                me.__treeHistoryCursor--;
                var newSelection = me.__treeHistory[me.__treeHistoryCursor];
                me.__tree.getTree().setSelection([newSelection]);
                me.__treeCurrentSelection = newSelection;
                newSelection.setOpen(true);
                if (newSelection.getUserData('fileRef') === undefined) {
                    me.__list.setFiles(me.__fileTree.children, me.__fileTree);
                    me.__refreshButton.setEnabled(false);
                    me.__addFolderButton.setEnabled(false);
                    me.__removeButton.setEnabled(false);
                }
                else {
                    me.__list.setFiles(
                        newSelection.getUserData('fileRef').getChildren(),
                        newSelection.getUserData('fileRef'));
                    me.__list.getList().resetSelection();
                    me.__refreshButton.setEnabled(true);
                    me.__addFolderButton.setEnabled(true);
                    me.__removeButton.setEnabled(true);
                }
                me.__editButton.setEnabled(false);
                if (me.__treeHistoryCursor === 0) {
                    me.__backButton.setEnabled(false);
                }
                if (me.__treeHistoryCursor < me.__treeHistory.length) {
                    me.__forwardButton.setEnabled(true);
                }
            }
        },

        __forwardFolder: function() {
            var me = this;
            if (me.__treeHistoryCursor >= 0 &&
                me.__treeHistoryCursor < me.__treeHistory.length - 1) {
                me.__treeHistoryCursor++;
                var newSelection = me.__treeHistory[me.__treeHistoryCursor];
                me.__tree.getTree().setSelection([newSelection]);
                me.__treeCurrentSelection = newSelection;
                newSelection.setOpen(true);
                if (newSelection.getUserData('fileRef') === undefined) {
                    me.__list.setFiles(
                        me.__fileTree.children,
                        me.__fileTree);
                    me.__refreshButton.setEnabled(false);
                    me.__addFolderButton.setEnabled(false);
                    me.__removeButton.setEnabled(false);
                    me.__editButton.setEnabled(false);
                }
                else {
                    me.__list.setFiles(
                        newSelection.getUserData('fileRef').getChildren(),
                        newSelection.getUserData('fileRef'));
                    me.__list.getList().resetSelection();
                    me.__refreshButton.setEnabled(true);
                    me.__addFolderButton.setEnabled(true);
                    me.__removeButton.setEnabled(true);
                }
                me.__editButton.setEnabled(false);
                if (me.__treeHistoryCursor === me.__treeHistory.length - 1) {
                    me.__forwardButton.setEnabled(false);
                }
                if (me.__treeHistory.length > 1) {
                    me.__backButton.setEnabled(true);
                }
            }
        },

        __addFolder: function() {
            var me = this;
            me.__addFolderButton.setEnabled(false);
            if (me.__treeCurrentSelection) {
                me.__list.addFolder(
                    me.__treeCurrentSelection.getUserData('fileRef')
                );
            }
        },

        __addFolderHandler: function(evt) {
            if (evt.getData().success) {
                return;
            }
        },

        __upload: function() {
            var me = this;
            me.__uploader.__uploadHandler.__uploadFolder =
                me.__treeCurrentSelection.getUserData('fileRef');
            return;
        },

        __uploadHandler: function() {
        },

        __saveStart: function() {
            var me = this;
            // me.__uploadButton.setEnabled(false);
        },

        __saveCompleted: function() {
            var me = this;
            // me.__uploadButton.setEnabled(true);
        },

        __download: function() {
            var me = this;
            if (me.__list.getList().getSelectionModel().isSelectionEmpty()) {
                return;
            }
            this.__downloadButton.setEnabled(false);
            if (me.__treeCurrentSelection) {
                me.__list.downloadItems(
                    me.__treeCurrentSelection.getUserData('fileRef'));
            }
        },

        __downloadHandler: function(evt) {
            this.__downloadButton.setEnabled(true);
            if (evt.getData().success) {
                var type = evt.getData().file.getMimeType();
                var name = evt.getData().file.getName();
                var content = evt.getData().content;
                var blob = new Blob([content], {type: type});
                saveAs(blob, name);
            }
        },

        __remove: function() {
            var me = this;
            var selection = me.__list.getList().getSelectionModel().getSelectedRanges();
            var ndxs = [];
            for (var i = 0; i < selection.length; i++) {
                var min = selection[i].minIndex;
                var max = selection[i].maxIndex;
                for (var j = min; j <= max; j++) {
                    ndxs.push(me.__list.getModel().getRowData(j)[3]);
                }
            }
            var filemanager = everydesk.base.system.FileManager.getInstance();
            filemanager.rm(ndxs);
        },

        __removeHandler: function(evt) {
            if (evt.getData().success) {
                return;
            }
        },

        __edit: function() {
            var me = this;
            if (me.__list.getList().getSelectionModel().isSelectionEmpty()) {
                return;
            }
            this.__editButton.setEnabled(false);
            if (me.__treeCurrentSelection) {
                me.__list.editItem(
                    me.__treeCurrentSelection.getUserData('fileRef')
                );
            }
        },

        __editHandler: function(evt) {
            if (evt.getData().success) {
                // var node = me.__tree.getNode(evt.getData().file.getPath(), me.__tree.getRoot());
                // node.removeAll();
                return;
            }
        },

        __refreshFileBrowser: function(evt) {
            if (evt && evt.getData().type !== 'file') {
                return;
            }
            this.__tree.getRoot().removeAll();
            this.loadFileSystems();
        },

        loadFileSystems: function() {
            var me = this;
            var fs = me.__fileTree;

            for (var i = 0; i < fs.children.length; i++) {
                me.__tree.addNode(me.__tree.getRoot(), fs.children[i]);
                me.__addNodes(fs.children[i].getPath(), fs.children[i].getChildren(), me.__tree.getRoot());
            }
            if (me.__treeCurrentSelection) {
                var file = me.__treeCurrentSelection.getUserData('fileRef');
                var treeNode = me.__tree.getNode(
                    file.getPath(),
                    me.__tree.getRoot());
                if (treeNode) {
                    me.__tree.getTree().setSelection([treeNode]);
                    me.__treeCurrentSelection = treeNode;
                    me.__treeHistory.push(treeNode);
                    me.__treeHistoryCursor++;
                    me.__list.getModel().removeRows(0, me.__list.getModel().getRowCount());
                    me.__list.setFiles(
                        treeNode.getUserData('fileRef').getChildren(),
                        treeNode.getUserData('fileRef'));
                    treeNode.setOpen(true);
                }
            }
        },

        __addNodes: function(parent, items, node) {
            var me = this;
            var pNode = me.__tree.getNode(parent, node);
            for (var i = 0; i < items.length; i++) {
                me.__tree.addNode(pNode, items[i]);
                if (items[i].getType() === 'folder') {
                    me.__addNodes(items[i].getPath(), items[i].getChildren(), me.__tree.getRoot());
                }
            }
        },

        __getFileNode: function(path, node) {
            var parts = path.split('/');
            if (path === '/fs/') {
                return node;
            }

            var children = node.getChildren();
            var ndx = 2;
            for (var i = 0; i < children.length; i++) {
                if (children[i].getName() === decodeURIComponent(parts[ndx]) &&
                    ndx === parts.length - 2) {
                    return children[i];
                }
                else if (children[i].getName() === decodeURIComponent(parts[ndx])) {
                    var current = children[i];
                    while (current) {
                        var currChildren = current.children;
                        ndx++;
                        for (var j = 0; j < currChildren.length; j++) {
                            if (currChildren[j].getName() === decodeURIComponent(parts[ndx]) &&
                                (ndx === parts.length - 1 ||
                                ndx === parts.length - 2 && parts[parts.length - 1] === '')) {
                                return currChildren[j];
                            }
                            else if (currChildren[j].getName() === decodeURIComponent(parts[ndx])) {
                                current = currChildren[j];
                                break;
                            }
                        }
                    }
                }
            }
        },

        refresh: function() {
            var me = this;
            me.__treeHistoryCursor = -1;
            me.__treeHistory.length = 0;
            me.__forwardButton.setEnabled(false);
            me.__backButton.setEnabled(false);
            var filemanager = everydesk.base.system.FileManager.getInstance();
            if (me.__treeCurrentSelection) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'file',
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                var node = me.__treeCurrentSelection;
                while (node.getLevel() > 0) {
                    node = node.getParent();
                }
                filemanager.refresh(
                    me.__treeCurrentSelection.getUserData('fileRef'),
                    node.getUserData('fileRef').getName());
            }
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.setWidth(600);
        me.setHeight(400);
        me.init();
        me.centerWindow();
        qx.event.message.Bus.subscribe('actionend', me.__refreshFileBrowser, me);
        qx.event.message.Bus.subscribe('file:rm', me.__removeHandler, me);
        qx.event.message.Bus.subscribe('file:get', me.__downloadHandler, me);
        qx.event.message.Bus.subscribe('file:mkdir', me.__addFolderHandler, me);
        qx.event.message.Bus.subscribe('file:mv', me.__editHandler, me);
        qx.event.message.Bus.subscribe('file:save', me.__uploadHandler, me);
        qx.event.message.Bus.subscribe('file:__savecompleted', me.__saveCompleted, me);
        qx.event.message.Bus.subscribe('file:__savestarted', me.__saveStart, me);
    }
});
