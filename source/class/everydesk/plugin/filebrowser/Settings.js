/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.filebrowser.Settings', {
    extend: qx.core.Object,

    members: {
        __services: null,
        __form: null,
        __formModel: null,
        __controller: null,
        __currentService: null,

        getUI: function() {
            var me = this;
            var container = new qx.ui.container.Composite();
            this.__services = everydesk.base.system.FileManager.getInstance().getServices();
            var list = new qx.ui.form.List();
            var layout = new qx.ui.layout.VBox();
            container.setLayout(layout);
            container.add(list, {flex: 1});
            for (var i = 0; i < this.__services.length; i++) {
                list.add(new qx.ui.form.ListItem(this.__services[i].name));
            }
            list.addListener('changeSelection', function(evt) {
                var name = evt.getData()[0].getLabel();
                var ndx = 0;
                for (var j = 0; j < this.__services.length; j++) {
                    if (name === this.__services[j].name) {
                        ndx = j;
                        break;
                    }
                }
                container.removeAt(2);
                container.add(this.__buildForm(this.__services[ndx]), {flex: 2});
                this.__updateForm(this.__services[ndx]);
            }, this);
            var tools = new qx.ui.toolbar.ToolBar();
            var newMenuButton = new qx.ui.toolbar.MenuButton('New');
            var newMenu = new qx.ui.menu.Menu();
            var newWebdav = new qx.ui.menu.Button('Webdav');
            newWebdav.addListener('execute', function() {
                container.removeAt(2);
                me.__currentService = {};
                me.__currentService.id = undefined;
                me.__currentService.type = 'webdav';
                me.__currentService.name = '';
                me.__currentService.user = '';
                me.__currentService.url = '';
                me.__currentService.path = 'fs-' + me.__services.length + 1;
                container.add(me.__buildForm(me.__currentService), {flex: 2});
                me.__controller.resetModel();
                me.__formModel = me.__controller.createModel();
                me.__updateForm(me.__currentService);
            });
            var newDropbox = new qx.ui.menu.Button('Dropbox');
            newDropbox.addListener('execute', function() {
                container.removeAt(2);
                me.__currentService = {};
                me.__currentService.id = undefined;
                me.__currentService.type = 'dropbox';
                me.__currentService.name = '';
                me.__currentService.path = 'fs-' + me.__services.length + 1;
                container.add(me.__buildForm(me.__currentService), {flex: 2});
                me.__controller.resetModel();
                me.__formModel = me.__controller.createModel();
                me.__updateForm(me.__currentService);
            });
            newMenu.add(newWebdav);
            newMenu.add(newDropbox);
            newMenuButton.setMenu(newMenu);
            tools.addSpacer();
            tools.add(newMenuButton);
            container.add(tools);
            container.add(this.__buildForm(this.__services[0]), {flex: 2});
            return container;
        },

        __buildForm: function(service) {
            var me = this;
            var groupBox = new qx.ui.groupbox.GroupBox('Service');
            groupBox.setLayout(new qx.ui.layout.Canvas());
            var saveButton = new qx.ui.form.Button('Save');
            var dbButton = new qx.ui.form.Button('Request Access-Key');
            this.__form = new qx.ui.form.Form();
            var type = new qx.ui.form.TextField();
            type.setReadOnly(true);
            this.__form.add(type, 'Type', null, 'type');
            this.__form.add(new qx.ui.form.TextField(), 'Name', null, 'name');
            if (service && service.type === 'webdav') {
                this.__form.add(new qx.ui.form.TextField(), 'Url', null, 'url');
                this.__form.add(new qx.ui.form.TextField(), 'User', null, 'user');
            }
            this.__form.add(new qx.ui.form.PasswordField(), 'Password', null, 'password');
            if (service && service.type === 'dropbox') {
                this.__form.addButton(dbButton);
            }
            this.__form.addButton(saveButton);
            this.__controller = new qx.data.controller.Form(null, this.__form);
            this.__formModel = this.__controller.createModel();
            dbButton.addListener('execute', function(evt) {
                everydesk.base.filesystem.Dropbox.requestAccessToken();
            });
            saveButton.addListener('execute', function() {
                me.__currentService.name = me.__formModel.getName();
                me.__currentService.password = me.__formModel.getPassword();
                var type = me.__formModel.getType();
                me.__currentService.type = {};
                me.__currentService.type.name = type;
                if (type === 'webdav') {
                    me.__currentService.url = me.__formModel.getUrl();
                    me.__currentService.login = me.__formModel.getUser();
                    me.__currentService.password = me.__formModel.getPassword();
                    var tmpSub = me.__formModel.getUrl().split('://')[1];
                    var sourcePath = tmpSub.substring(tmpSub.indexOf('/'), tmpSub.length);
                    me.__currentService.sourcePath = sourcePath;
                }
                else {
                    me.__currentService.url = '';
                    me.__currentService.login = '';
                    me.__currentService.password = '';
                    me.__currentService.sourcePath = '';
                }
                me.__saveFileservice(me.__currentService);
            });
            groupBox.add(new qx.ui.form.renderer.Single(this.__form));
            return groupBox;
        },

        __saveFileservice: function(service) {
            var request = new everydesk.base.io.Xhr('/fileservices', 'POST');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.setRequestHeader(
                'Content-Type', 'application/json'
            );
            request.addListener('success', function(evt) {
                console.log(evt);
            });
            request.addListener('fail', function() {
            });
            request.setRequestData(service);
            request.send();
        },

        __updateForm: function(service) {
            this.__currentService = service;
            this.__formModel.setName(service.name);
            this.__formModel.setType(service.type);
            if (service.type === 'webdav') {
                this.__formModel.setUrl(service.url);
                this.__formModel.setUser(service.user);
            }
        }
    },

    construct: function() {
        this.base(arguments);
    }
});

