/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.plugin.filebrowser.FileList', {
    extend: qx.ui.container.Composite,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __table: null,
        __model: null,
        __currentFolder: null,

        init: function() {
            var me = this;
            me.setLayout(new qx.ui.layout.VBox());
            me.__model = new qx.ui.table.model.Filtered();
            me.__model.setColumnIds(['icon', 'displayName', 'size']);
            me.__model.setColumns(['', 'Name', 'Size']);
            var colModel = {
                tableColumnModel: function(obj) {
                    return new qx.ui.table.columnmodel.Resize(obj);
                }
            };
            me.__table = new qx.ui.table.Table(me.__model, colModel);
            me.__table.setStatusBarVisible(false);
            me.__table.setShowCellFocusIndicator(false);
            me.__table.setFocusCellOnPointerMove(true);
            me.__table.setColumnVisibilityButtonVisible(false);
            me.__table.getDataRowRenderer().setHighlightFocusRow(true);
            me.__table.getSelectionModel().setSelectionMode(4);
            me.__table.setFocusable(true);
            me.__table.setDraggable(true);
            me.__table.setDroppable(true);
            me.__table.addListener('dragstart', function(evt) {
                var selected =
                    me.__table.getSelectionModel().getSelectedRanges();
                var row = me.getCellUnderMouse(me.__table, evt);
                if (row.row === -1) {
                    evt.stopSession();
                    return;
                }
                if (selected.length === 0) {
                    me.__table.getSelectionModel().addSelectionInterval(
                        row.row,
                        row.row);
                }
                else {
                    var selectionContains = false;
                    for (var i = 0; i < selected.length; i++) {
                        if (row.row >= selected[i].minIndex &&
                            row.row <= selected[i].maxIndex) {
                            selectionContains = true;
                        }
                    }
                    if (!selectionContains) {
                        me.__table.getSelectionModel().resetSelection();
                        me.__table.getSelectionModel().addSelectionInterval(
                            row.row,
                            row.row);
                    }
                }
                evt.addType('file');
                evt.addAction('move');
            });
            me.__table.addListener('dragend', function() {
                me._getDragDropCursor().resetAction();
                me._getDragDropCursor().moveTo(-1000, -1000);
                me.getApplicationRoot().resetGlobalCursor();
            });
            me.__table.addListener('dragover', function(evt) {
                if (!evt.supportsType('file')) {
                    evt.preventDefault();
                }
            });
            me.__table.addListener('dragleave', function(evt) {
                evt.getCurrentTarget().resetBackgroundColor();
                evt.getCurrentTarget().resetTextColor();
            });
            me.__table.addListener('dragchange', function() {
            });
            me.__table.addListener('drag', function(evt) {
                if (evt.getRelatedTarget() === null) {
                    return;
                }
                var target = evt.getRelatedTarget();
                if (target instanceof qx.ui.table.Table) {
                    target.focus();
                }
            });
            me.__table.addListener('drop', function(evt) {
                var data = evt.getData('file');
                var row = me.getCellUnderMouse(me.__table, evt);
                var target;
                if (row.row === -1) {
                    target = me.__currentFolder;
                }
                else {
                    target = me.__model.getRowData(row.row)[3];
                }
                var filemanager =
                    everydesk.base.system.FileManager.getInstance();
                for (var i = 0; i < data.length; i++) {
                    if (data[i].getPath() === target.getPath()) {
                        continue;
                    }
                    filemanager.mv(
                        data[i],
                        target.getPath() + data[i].getDisplayName());
                }
            });
            me.__table.addListener('droprequest', function(evt) {
                var selectionModel = evt.getCurrentTarget().getSelectionModel();
                var selection = [];
                selectionModel.iterateSelection(function(ndx) {
                    selection.push(me.__model.getRowData(ndx)[3]);
                });
                evt.addData('file', selection);
            });
            var tcm = me.__table.getTableColumnModel();
            tcm.setDataCellRenderer(
                0,
                new qx.ui.table.cellrenderer.Image(16, 16));
            tcm.getBehavior().setWidth(0, 26);
            tcm.getBehavior().setWidth(1, '1*');
            tcm.getBehavior().setWidth(2, 60);
            me.__model.setColumnEditable(1, true);
            me.add(me.__table, {flex: 1});
        },

        getCellUnderMouse: function(table, evt) {
            var row = -1;
            var col = -1;
            if (table && evt) {
                var pageX = evt.getDocumentLeft();
                var pageY = evt.getDocumentTop();
                var sc = table.getTablePaneScrollerAtPageX(pageX);
                if (sc) {
                    row = sc._getRowForPagePos(pageX, pageY);
                    col = sc._getColumnForPageX(pageX);
                    if ((row === null) || (row === undefined)) {
                        row = -1;
                    }
                    if ((col === null) || (col === undefined)) {
                        col = -1;
                    }
                }
            }
            return ({row: row, col: col});
        },

        downloadItems: function(parentFolder) {
            var me = this;
            var selectionModel = me.__table.getSelectionModel();
            var selection = [];
            selectionModel.iterateSelection(function(ndx) {
                selection.push(me.__model.getRowData(ndx)[3]);
            });
            var filemanager = everydesk.base.system.FileManager.getInstance();
            filemanager.get(parentFolder, selection);
        },

        addFolder: function(folder) {
            var me = this;
            var newItem = [[
                me.__getTypeIcon('folder'),
                'New Folder',
                undefined,
                folder
            ]];
            me.__editedListener = me.__table.addListenerOnce(
                'dataEdited',
                function(evt) {
                    var filemanager =
                        everydesk.base.system.FileManager.getInstance();
                    var parent = me.__model.getRowData(evt.getData().row)[3];
                    filemanager.mkdir(parent, evt.getData().value);
                    me.__table.clearFocusedRowHighlight();
                    me.__table.removeListenerById(me.__listenerId);
                    me.__table.removeListenerById(me.__editedListener);
                }
            );
            me.__listenerId = me.__table.addListener('keypress', function(evt) {
                if (!me.__table.isEnabled()) {
                    return;
                }
                if (evt.getKeyIdentifier() === 'Escape') {
                    me.__table.stopEditing();
                    me.__table.clearFocusedRowHighlight();
                    var selected =
                        me.__table.getSelectionModel().getSelectedRanges();
                    me.__model.removeRows(selected[0].minIndex, 1);
                    me.__table.removeListenerById(me.__listenerId);
                    me.__table.removeListenerById(me.__editedListener);
                    qx.event.message.Bus.dispatch(new qx.event.message.Message(
                        'file:mkdir', {
                            success: false,
                            file: null
                        }
                    ));
                }
            });
            me.__model.addRows(newItem);
            me.__table.getSelectionModel().resetSelection();
            me.__table.getSelectionModel().addSelectionInterval(
                me.__model.getRowCount() - 1, me.__model.getRowCount() - 1);
            me.__model.setColumnEditable(1, true);
            me.__table.setFocusedCell(1, me.__model.getRowCount() - 1);
            me.__table.startEditing();
        },

        editItem: function(parentFolder) {
            var me = this;
            me.__model.setColumnEditable(1, true);
            me.__editedListener = me.__table.addListenerOnce(
                'dataEdited',
                function(evt) {
                    me.__table.stopEditing();
                    var oldValue = evt.getData().oldValue;
                    var newValue = evt.getData().newValue;
                    if (oldValue !== newValue) {
                        var filemanager =
                            everydesk.base.system.FileManager.getInstance();
                        var orig = me.__model.getRowData(evt.getData().row)[3];
                        filemanager.mv(
                            orig, parentFolder.getPath() + evt.getData().value);
                    }
                    me.__table.clearFocusedRowHighlight();
                    me.__table.removeListenerById(me.__listenerId);
                    me.__table.removeListenerById(me.__editedListener);
                });
            me.__listenerId = me.__table.addListener('keypress', function(evt) {
                if (!me.__table.isEnabled()) {
                    return;
                }
                if (evt.getKeyIdentifier() === 'Escape') {
                    me.__table.stopEditing();
                    me.__table.clearFocusedRowHighlight();
                    me.__table.removeListenerById(me.__listenerId);
                    me.__table.removeListenerById(me.__editedListener);
                    qx.event.message.Bus.dispatch(new qx.event.message.Message(
                        'file:mv', {
                            success: false,
                            file: null
                        }
                    ));
                }
            });
            var selection = me.__table.getSelectionModel().getSelectedRanges();
            me.__table.getSelectionModel().resetSelection();
            me.__table.getSelectionModel().addSelectionInterval(
                selection[0].minIndex,
                selection[0].minIndex);
            me.__table.setFocusedCell(1, selection[0].minIndex);
            me.__table.startEditing();
        },

        getList: function() {
            return this.__table;
        },

        getModel: function() {
            return this.__model;
        },

        setFiles: function(data, folder) {
            var me = this;
            me.__currentFolder = folder;
            var items = [];
            for (var i = 0; i < data.length; i++) {
                var size = me.__formatSize(data[i].getSize());
                var icon = me.__getTypeIcon(data[i].getMimeType());
                items.push([icon, data[i].getDisplayName(), size, data[i]]);
            }
            this.__model.setData(items);
        },

        __formatSize: function(size) {
            if (!size) {
                return '';
            }
            var a = ['byte', 'KB', 'MB', 'GB'];
            var value = size;
            var tmp = size;
            var i = 0;
            while (tmp > 1) {
                tmp = tmp/1000;
                if (tmp > 1) {
                    i++;
                    value = tmp;
                }
            }
            if (i > 0) {
                value = value.toFixed(2);
            }
            return value + ' ' + a[i];
        },

        __getTypeIcon: function(type) {
            var baseUrl = 'resource/everydesk/icons/16x16/';
            return baseUrl +
                everydesk.plugin.filebrowser.Filebrowser.FILEICONS[type];
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.init();
    }
});
