/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.plugin.filebrowser.FileOpen', {
    extend: everydesk.plugin.Dialog,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __list: null,
        __fileTree: null,
        __currentFolder: null,
        __filebar: null,
        __toolbarFilePart: null,
        __currentLayer: 0,
        __layers: [],

        init: function() {
            var me = this;
            me.base(arguments);
            me.setCaption('Open file');
            me.__fileTree = everydesk.base.system.FileManager.getInstance().getFileSystem();
            me.__fileGroup = new qx.ui.form.RadioGroup();
            me.__fileGroup.setAllowEmptySelection(false);
            var refreshButton = new qx.ui.toolbar.Button(null, 'everydesk/icons/16x16/arrow_refresh.png');

            me.__filebar = new everydesk.plugin.filebrowser.FileBar();
            me.__filebar.add(refreshButton);
            me.__filebar.setOverflowHandling(true);
            me.add(me.__filebar);

            var mainLayout = new qx.ui.layout.VBox();
            me.setLayout(mainLayout);
            me.__list = new everydesk.plugin.filebrowser.FileList();
            me.__list.getList().addListener('dblclick', function(evt) {
                evt.stopPropagation();
                var selectionModel = evt.getCurrentTarget().getSelectionModel();
                if (selectionModel.getSelectedCount() > 0) {
                    var ndx = selectionModel.getSelectedRanges()[0].maxIndex;
                    var file = me.__list.getModel().getRowData(ndx)[3];
                    if (file && file.getType() === 'file') {
                        me.__layers.splice(0, me.__layers.length);
                        me.__currentLayer = 0;
                        me.__referer.open(file);
                        me.close();
                    }
                    else if (file && file.getType() === 'folder') {
                        me.__list.setFiles(file.getChildren());
                        me.__list.getList().resetSelection();
                        me.__filterbar.applyFilter();
                        me.__addFolderButton(file);
                    }
                }
            }, me, true);
            me.add(me.__list, {
                flex: 1
            });

            me.__filterbar = new everydesk.plugin.filebrowser.FilterBar();
            me.__filterbar.setModel(me.__list.getModel());
            // me.__list.addListener('appear', me.__filterbar.applyFilter, me.__filterbar);
            me.add(me.__filterbar);
            me.open('/fs/');
            me.__buildFilebar();
        },

        __buildFilebar: function() {
            var me = this;
            var root = new qx.ui.toolbar.RadioButton(me.__fileTree.name);
            root.setUserData('ndx', 0);
            root.addListener('execute', me.__open, me);
            me.__layers = [{btn: root, file: me.__fileTree}];
            me.__filebar.add(root);
            me.__fileGroup.add(root);
            me.__currentLayer = 0;
        },

        __addFolderButton: function(file) {
            var me = this;
            me.__filebar.setOverflowHandling(false);
            me.__filebar.clearOverflow();
            me.__currentLayer++;
            if (me.__currentLayer < me.__layers.length) {
                for (var i = me.__currentLayer; i < me.__layers.length; i++) {
                    me.__filebar.remove(me.__layers[i].btn);
                }
                me.__layers.splice(me.__currentLayer, me.__layers.length);
            }
            var button = new qx.ui.toolbar.RadioButton(file.getName());
            button.setUserData('ndx', me.__currentLayer);
            button.addListener('execute', me.__open, me);
            me.__fileGroup.add(button);
            me.__layers.push({btn: button, file: file});
            me.__filebar.add(button);
            button.toggleValue();
            me.__filebar.setOverflowHandling(true);
        },

        __open: function(evt) {
            var me = this;
            var ndx = evt.getCurrentTarget().getUserData('ndx');
            me.__currentLayer = ndx;
            var select = me.__layers[ndx];
            if (select.file.path) {
                me.open(select.file.path);
            }
            else {
                me.open(select.file.getPath());
            }
        },

        setReferer: function(referer) {
            this.__referer = referer;
            this.__filterbar.setFileTypes(referer.getFileTypes());
        },

        open: function(path) {
            var me = this;
            var file = me.__getFileNode(path, me.__fileTree);
            if (file.children) {
                me.__list.setFiles(file.children);
            }
            else {
                me.__list.setFiles(file.getChildren());
            }
            me.__filterbar.applyFilter();
        },

        __getFileNode: function(path, node) {
            var parts = path.split('/');
            if (path === '/fs/') {
                return node;
            }

            var children = node.children;
            var ndx = 2;

            for (var i = 0; i < children.length; i++) {
                var nodeParts = children[i].getPath().split('/');
                if (parts[ndx] === nodeParts[ndx] && ndx === parts.length - 2) {
                    return children[i];
                }
                else if (parts[ndx] === nodeParts[ndx]) {
                    var current = children[i];
                    while (current && ndx < parts.length) {
                        ndx++;
                        var currChildren = current.getChildren();
                        for (var j = 0; j < currChildren.length; j++) {
                            var childParts = currChildren[j].getPath().split('/');
                            if (childParts[ndx] === parts[ndx] &&
                                ndx === parts.length - 2) {
                                return currChildren[j];
                            }
                            else if (childParts[ndx] === parts[ndx]) {
                                current = currChildren[j];
                                break;
                            }
                        }
                    }
                }
            }
        }
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        me.setWidth(400);
        me.setHeight(300);
        me.init();
    }
});
