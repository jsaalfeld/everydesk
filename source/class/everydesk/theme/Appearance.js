/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

qx.Theme.define('everydesk.theme.Appearance', {
    extend: qx.theme.modern.Appearance,

    appearances: {
        'mainmenubutton': {
            style: function(states) {
                var decorator;
                var textColor;
                var padding = [3, 9]; // default padding

                if (states.checked && states.focused && !states.inner) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else if (states.disabled) {
                    decorator = 'button-disabled';
                    textColor = null;
                }
                else if (states.pressed) {
                    decorator = 'mainmenubutton-pressed';
                    textColor = null;
                }
                else if (states.checked) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else if (states.hovered) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else if (states.focused && !states.inner) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }

                if (states.invalid && !states.disabled) {
                    decorator += '-invalid';
                }

                return {
                  decorator: decorator,
                  textColor: textColor,
                  padding: padding,
                  margin: [1, 0]
                };
            }
        },
        'traybutton': {
            style: function(states) {
                var decorator;
                var textColor;
                var padding = [3, 3]; // default padding

                if (states.checked && states.focused && !states.inner) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else if (states.disabled) {
                    decorator = 'button-disabled';
                    textColor = null;
                }
                else if (states.pressed) {
                    decorator = 'mainmenubutton-pressed';
                    textColor = null;
                }
                else if (states.checked) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else if (states.hovered) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else if (states.focused && !states.inner) {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }
                else {
                    decorator = 'mainmenubutton';
                    textColor = null;
                }

                if (states.invalid && !states.disabled) {
                    decorator += '-invalid';
                }

                return {
                  decorator: decorator,
                  textColor: textColor,
                  padding: padding,
                  margin: [1, 0]
                };
            }
        },
        'startermenu': {
            style: function(states) {
                var decorator;
                var textColor;
                var padding = [3, 9]; // default padding

                if (states.checked && states.focused && !states.inner) {
                    decorator = 'startermenu';
                    textColor = null;
                }
                else if (states.disabled) {
                    decorator = 'button-disabled';
                    textColor = null;
                }
                else if (states.pressed) {
                    decorator = 'startermenu-pressed';
                    textColor = null;
                }
                else if (states.checked) {
                    decorator = 'startermenu-pressed';
                    textColor = null;
                }
                else if (states.hovered) {
                    decorator = 'startermenu-hovered';
                    textColor = null;
                }
                else if (states.focused && !states.inner) {
                    decorator = 'startermenu';
                    textColor = null;
                }
                else {
                    decorator = 'startermenu';
                    textColor = null;
                }

                if (states.invalid && !states.disabled) {
                    decorator += '-invalid';
                }

                return {
                    decorator: decorator,
                    textColor: textColor,
                    padding: padding,
                    margin: [1, 0]
                };
            }
        },
        'starterbutton': {
            style: function(states) {
                var decorator;
                var textColor;
                var padding = [3, 9]; // default padding

                if (states.checked && states.focused && !states.inner) {
                    decorator = 'starterbutton';
                    textColor = null;
                }
                else if (states.disabled) {
                    decorator = 'button-disabled';
                    textColor = null;
                }
                else if (states.pressed) {
                    decorator = 'starterbutton-pressed';
                    textColor = null;
                }
                else if (states.checked) {
                    decorator = 'starterbutton-pressed';
                    textColor = null;
                }
                else if (states.hovered) {
                    decorator = 'starterbutton-hovered';
                    textColor = null;
                }
                else if (states.focused && !states.inner) {
                    decorator = 'starterbutton';
                    textColor = null;
                }
                else {
                    decorator = 'starterbutton';
                    textColor = null;
                }

                if (states.invalid && !states.disabled) {
                    decorator += '-invalid';
                }

                return {
                  decorator: decorator,
                  textColor: textColor,
                  padding: padding,
                  margin: [1, 0]
                };
            }
        },
        'taskbutton': {
            style: function(states) {
                var decorator;
                var textColor;
                var padding = [3, 9]; // default padding

                if (states.checked && states.focused && !states.inner) {
                    decorator = 'taskbutton-pressed';
                    textColor = null;
                }
                else if (states.disabled) {
                    decorator = 'button-disabled';
                    textColor = null;
                }
                else if (states.pressed) {
                    decorator = 'taskbutton-pressed';
                    textColor = null;
                }
                else if (states.checked) {
                    decorator = 'taskbutton-pressed';
                    textColor = null;
                }
                else if (states.hovered) {
                    decorator = 'taskbutton-hovered';
                    textColor = null;
                }
                else if (states.focused && !states.inner) {
                    decorator = 'taskbutton';
                    textColor = null;
                }
                else {
                    decorator = 'taskbutton';
                    textColor = null;
                }

                if (states.invalid && !states.disabled) {
                    decorator += '-invalid';
                }

                return {
                  decorator: decorator,
                  textColor: textColor,
                  padding: padding,
                  margin: [1, 0]
                };
            }
        },
        "window/captionbar": {
          style : function(states)
          {
            return {
              minHeight : 26,
              paddingRight : 2,
          decorator : (states.active ? "window-captionbar-active" :
            "window-captionbar-inactive"),
              textColor : states.active ? "window-active-caption-text" : "window-inactive-caption-text"
            };
          }
        }
    }
});
