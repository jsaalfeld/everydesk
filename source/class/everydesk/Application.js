/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This is the main application class of your custom application "everydesk"
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.Application', {
    extend: qx.application.Standalone,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */

    members: {
        baseStack: null,
        apps: null,
        fileservices: null,

        /**
         * This method contains the initial application code and gets called
         * during startup of the application
         *
         * @lint ignoreDeprecated(alert)
         */
        main: function() {
            // Call super class
            this.base(arguments);

            // Enable logging in debug variant
            if (qx.core.Environment.get('qx.debug')) {
                // support native logging capabilities, e.g. Firebug for Firefox
                qx.log.appender.Native;
                // support additional cross-browser console.
                // Press F7 to toggle visibility
                qx.log.appender.Console;
            }

            everydesk.base.system.TaskManager.getInstance();
            // Document is the application root
            var doc = this.getRoot();
            doc.setBackgroundColor('#6495ed');

            this.baseStack = new qx.ui.container.Stack();
            doc.add(this.baseStack, {left: 0, top: 0, bottom: 0, right: 0});

            var login = new everydesk.base.screen.Login();
            login.addListener('loginSuccess', this.loginSuccess, this);
            this.baseStack.add(login);
            this.baseStack.add(new everydesk.base.screen.Desktop());
        },

        loginSuccess: function(evt) {
            var me = this;
            everydesk.base.system.Session.USERNAME = evt.getData().user;
            everydesk.base.system.Session.SESSIONKEY = evt.getData().key;
            me.debug('Login successful');
            me.debug('User: ' + everydesk.base.system.Session.USERNAME);
            me.apps = everydesk.base.system.StarterManager.getInstance();
            me.apps.loadApps();
            me.fileservices = everydesk.base.system.FileManager.getInstance();
            me.fileservices.load();
            var ani = me.baseStack.getSelection()[0].fadeOut(1000);
            ani.addListener('end', function() {
                me.baseStack.next();
                me.baseStack.getSelection()[0].fadeIn(1000);
            }, this);
        }
    }
});
