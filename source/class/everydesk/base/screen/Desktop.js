/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/background/*)
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.screen.Desktop', {
    extend: qx.ui.container.Composite,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        background: null,
        workspace: null,
        windowManager: null,
        mainBar: null,
        mainMenu: null,
        mainMenuVisible: false,

        focusoutMainMenu: function(evt) {
            var me = this;
            var left = evt.getDocumentLeft();
            var top = evt.getDocumentTop();
            var bounds = me.mainMenu.getBounds();
            if (me.mainMenuVisible) {
                bounds.left = 0;
                if (left > bounds.left + bounds.width ||
                    top > bounds.top + bounds.height) {
                    me.hideMainMenu();
                }
            }
        },

        toggleMainMenu: function() {
            var me = this;
            if (me.mainMenuVisible) {
                me.hideMainMenu();
                return;
            }
            me.showMainMenu();
        },

        showMainMenu: function() {
            var me = this;
            var animation = {
                duration: 300,
                timing: 'ease-in',
                keep: 100,
                keyFrames: {
                    0: {
                        left: '-400px'
                    },
                    100: {
                        left: '0px'
                    }
                }

            };
            var handle = qx.bom.element.Animation.animate(
                me.mainMenu.getContentElement().getDomElement(), animation);
            handle.on('end', function() {
                me.mainMenu.getFocusElement().focus();
                me.mainMenuVisible = true;
            });
        },

        hideMainMenu: function() {
            var me = this;
            if (!me.mainMenuVisible) {
                return;
            }
            var animation = {
                duration: 300,
                timing: 'ease-out',
                keep: 100,
                keyFrames: {
                    0: {
                        left: '0px'
                    },
                    100: {
                        left: '-400px'
                    }
                }

            };
            var handle = qx.bom.element.Animation.animate(
                me.mainMenu.getContentElement().getDomElement(), animation);
            handle.on('end', function() {
                me.mainMenuVisible = false;
            });
        },

        __showApp: function(evt) {
            var me = this;
            var app = evt.getData();
            me.windowManager.add(app.getUi());
            app.getUi().show();
        },

        __showWidget: function(evt) {
            var me = this;
            var widget = evt.getData();
            me.workspace.add(widget);
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
    },

    /**
    * Create a new login screen.
    */
    construct: function() {
        var me = this;
        me.base(arguments);
        me.setBackgroundColor('#6495ed');

        me.background =
            new qx.ui.container.Composite(new qx.ui.layout.Canvas());
        me.workspace = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
        var bgDecorator = new qx.ui.decoration.Decorator();
        bgDecorator._tmpStyleBackgroundImage =
            bgDecorator._styleBackgroundImage;
        bgDecorator._styleBackgroundImage = function(styles) {
            bgDecorator._tmpStyleBackgroundImage(styles);
            styles['background-size'] = 'cover';
        };
        bgDecorator.setBackgroundImage('everydesk/background/air.jpg');
        bgDecorator.setBackgroundRepeat('no-repeat');
        me.background.set({
            decorator: bgDecorator
        });
        me.windowManager = new qx.ui.window.Desktop(new qx.ui.window.Manager());
        me.windowManager.setBackgroundColor('transparent');
        me.mainBar = new everydesk.base.widget.MainBar();
        me.actionBar = new everydesk.base.widget.ActionBar();
        me.mainBar.addListener('mainmenu', me.toggleMainMenu, me);
        me.mainMenu = new everydesk.base.widget.MainMenu();
        me.addListener('mouseup', me.focusoutMainMenu, me);

        me.setLayout(new qx.ui.layout.Canvas());
        me.add(me.background, {
            left: 0,
            top: 0,
            width: '100%',
            height: '100%'
        });
        me.add(me.workspace, {
            left: 0,
            top: 0,
            bottom: 35,
            width: '100%'
        });
        me.add(me.windowManager, {
            left: 0,
            top: 0,
            bottom: 35,
            width: '100%'
        });
        me.add(me.mainBar, {
            left: 0,
            bottom: 0,
            width: '100%'
        });
        me.add(me.actionBar, {
            right: 0,
            bottom: 35
        });
        me.add(me.mainMenu, {
            left: -400,
            top: 0,
            bottom: 35
        });

        qx.event.message.Bus.subscribe('taskstart', me.hideMainMenu, me);
        qx.event.message.Bus.subscribe('taskstart', me.__showApp, me);
    }
});
