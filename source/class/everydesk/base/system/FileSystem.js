/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.base.system.FileSystem', {
    extend: qx.core.Object,

    properties: {
        url: {
            check: 'String',
            nullable: false
        },
        sourcePath: {
            check: 'String',
            nullable: false
        },
        fileSystemPath: {
            check: 'String',
            nullable: false
        },
        basePath: {
            check: 'String',
            nullable: false
        }
    },
    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        ls: function(path, callback, scope) {
            this.__request(path, 'PROPFIND', undefined, callback, scope);
        },

        get: function(file, callback, scope) {
            var requestUri = file.getPath();

            var request = new everydesk.base.io.Xhr(requestUri, 'GET');
            request.addListener('load', function() {
                if (request.getStatus() === 200) {
                    callback.call(scope, true, file, request.getResponse());
                }
                else {
                    callback.call(scope, false, null, null);
                }
            });
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.setResponseType('arraybuffer');
            request.send();
        },

        mkdir: function(parent, name, callback, scope) {
            var requestUri = parent.getPath() + '/' + name;
            this.__request(requestUri, 'MKCOL', undefined, function(success) {
                callback.call(scope, success, parent);
            });
        },

        save: function(parent, name, content, callback, scope) {
            var requestUri = parent.getPath() + '/' + name;
            this.__request(requestUri, 'PUT', content, function(success) {
                callback.call(scope, success, parent, name);
            });
        },

        cp: function(source, destination, callback, scope) {
            var url = source.getPath();
            var request = new everydesk.base.io.Xhr(url, 'COPY');
            if (callback) {
                request.addListener('loadEnd', function() {
                    if (request.getReadyState() === 4 &&
                        request.getStatus() === 201) {
                        callback.call(scope, true, destination);
                    }
                    else {
                        callback.call(scope, false, null);
                    }
                });
            }
            request.setRequestHeader('Content-Type', 'text/xml; charset=UTF-8');
            request.setRequestHeader('Destination', this.getSourcePath() + destination);
            request.setRequestHeader('Overwrite', 'F');
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send();
        },

        rm: function(file, callback, scope) {
            var requestUri = file.getPath();
            this.__request(requestUri, 'DELETE', undefined, function(success) {
                callback.call(scope, success, file);
            });
        },

        mv: function(oldFile, newFile, callback, scope) {
            var url = oldFile.getPath();
            var request = new everydesk.base.io.Xhr(url, 'MOVE');
            if (callback) {
                request.addListener('loadEnd', function() {
                    if (request.getReadyState() === 4 &&
                        request.getStatus() === 200) {
                        oldFile.setName(newFile);
                        oldFile.setPath(oldFile.parent.path + newFile);
                        oldFile.setDisplayName(newFile);
                        callback.call(scope, true, newFile, oldFile);
                    }
                    else {
                        callback.call(scope, false, null);
                    }
                });
            }
            request.setRequestHeader('Content-Type', 'text/xml; charset=UTF-8');
            request.setRequestHeader('Destination', newFile);
            request.setRequestHeader('Overwrite', 'F');
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send();
        },

        __request: function(path, verb, content, callback, scope) {
            var me = this;
            var url = this.getUrl() + path;
            var request = new everydesk.base.io.Xhr(url, verb);
            if (callback) {
                request.addListener('loadEnd', function() {
                    if (request.getReadyState() === 4) {
                        if (verb === 'PROPFIND') {
                            if (request.getStatus() === 200) {
                                me.__parsePropfind(path, request.getResponse(), callback, scope);
                                return;
                            }
                            callback.call(scope, null, null, me.getBasePath());
                            return;
                        }
                        if (request.getStatus() === 200) {
                            callback.call(scope, true);
                        }
                        else {
                            callback.call(scope, false);
                        }
                    }
                });
            }
            request.setRequestHeader('X-EVERYDESK-SESSION', everydesk.base.system.Session.SESSIONKEY);
            request.send(content);
        },

        __parsePropfind: function(parent, entries, callback, scope) {
            var items = [];
            for (var i = 0; i < entries.length; i++) {
                items.push(new everydesk.base.system.File({
                    name: entries[i].name,
                    displayName: entries[i].name,
                    path: entries[i].path,
                    created: new Date(entries[i].created),
                    modified: new Date(entries[i].modified),
                    size: entries[i].size,
                    type: entries[i].type,
                    mimeType: entries[i].mimeType,
                    version: entries[i].version,
                    children: []
                }));
            }
            callback.call(scope, parent, items, this.getBasePath());
        },

        __extractProperties: function(node) {
            var properties = {};
            for (var i = 0; i < node.childNodes.length; i++) {
                var property = node.childNodes[i];
                if (property.namespaceURI !== 'DAV:') {
                    continue;
                }
                if (property.localName === 'getetag') {
                    properties.version = property.textContent;
                }
                if (property.localName === 'getlastmodified') {
                    properties.lastModified = new Date(property.textContent);
                }
                if (property.localName === 'quota-available-bytes') {
                    properties.quotaAvailable = Number(property.textContent);
                }
                if (property.localName === 'quota-used-bytes') {
                    properties.quotaUsed = Number(property.textContent);
                }
                if (property.localName === 'resourcetype' &&
                    property.childNodes[0] &&
                    property.childNodes[0].localName === 'collection') {
                    properties.type = 'folder';
                    properties.mimeType = 'folder';
                    properties.resourcetype = 1;
                }
                if (property.localName === 'getcontentlength') {
                    properties.size = Number(property.textContent);
                }
                if (property.localName === 'getcontenttype') {
                    properties.type = 'file';
                    properties.mimeType = property.textContent;
                }
            }
            if (properties.resourcetype === 1) {
                properties.getcontenttype = 'collection';
            }
            return properties;
        },

        __trimPath: function(content) {
            // remove trailing '/'
            var path = content.replace(/\/$/, '');
            if (path.lastIndexOf('/') === path.length - 1) {
                path = path.substring(0, path.length - 1);
            }
            if (path.lastIndexOf('/') < path.length - 1) {
                path = path.substring(
                    path.lastIndexOf('/') + 1,
                    path.length);
            }
            return decodeURIComponent(path);
        }
    },

    construct: function() {
    }
});
