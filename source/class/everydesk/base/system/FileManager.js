/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.FileManager', {
    extend: qx.core.Object,
    type: 'singleton',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        /**
         * The remote file services configured for the user.
         */
        __services: null,

        /**
         * The completely loaded file tree containing files from all services.
         */
        __fileTree: null,

        /**
         * The webdav connector.
         */
        __davFS: null,

        __dropbox: null,

        /**
         * Map of connectors and file services.
         */
        __connectors: {},


        __loadActive: 0,
        __loadCounter: 0,
        __removeCounter: 0,
        __maxLoads: 2,

        /**
         * State of the filemanager
         */
        //__state: [],

        __actionQueue: [],

        /**
         * Map of file system states
         *
         * {
         *     state: ('busy', 'idle'),
         *     loadCounter: (int)
         * }
         */
        __fsStates: {},

        getServices: function() {
            return this.__services;
        },

        /**
         * Request configured filesystems from server.
         */
        __getFileSystems: function() {
            var me = this;
            //me.__state.push({state: 'busy', object: 'self'});
            var request = new everydesk.base.io.Xhr('/fileservices', 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.__services = evt.getTarget().getResponse();
                me.__loadCounter--;
                me.__load();
            });
            request.addListener('fail', function() {
                if (me.__loadCounter < me.__maxLoads) {
                    me.__loadCounter++;
                    request.send();
                }
            });
            me.__loadCounter++;
            request.send();
        },

        /**
         * Load the configured filesystems.
         */
        __load: function() {
            var me = this;
            for (var i = 0; i < me.__services.length; i++) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'file',
                        name: me.__services[i].path,
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                me.__connectors[me.__services[i].path] = new everydesk.base.system.FileSystem();
                me.__connectors[me.__services[i].path].setUrl(window.location.origin);
                me.__connectors[me.__services[i].path].setBasePath(me.__services[i].path);
                me.__connectors[me.__services[i].path].setFileSystemPath('/fs');
                me.__fsStates[me.__services[i].path] = {};
                me.__fsStates[me.__services[i].path].state = 'busy';
                /*
                if (me.__services[i].type === 'webdav') {
                    var copy = me.__davFS.clone();
                    copy.setSourcePath(me.__services[i].sourcePath);
                    copy.setBasePath(me.__services[i].path);
                    me.__connectors[me.__services[i].path] = copy;
                    me.__fsStates[me.__services[i].path] = {};
                    me.__fsStates[me.__services[i].path].state = 'busy';
                }
                else if (me.__services[i].type === 'dropbox') {
                    var copy = me.__dropbox.clone();
                    copy.setBasePath(me.__services[i].path);
                    //copy.setToken(me.__services[i].password);
                    me.__connectors[me.__services[i].path] = copy;
                    me.__fsStates[me.__services[i].path] = {};
                    me.__fsStates[me.__services[i].path].state = 'busy';
                }
                */
                me.__loadFiles(me.__services[i]);
            }
        },

        /**
         * Load files recursive.
         */
        __loadFiles: function(service) {
            var me = this;
            var serviceNode;
            var name = service.name;
            var path = service.path;
            if (service.type.name === 'webdav') {
                var sourcePath = service.sourcePath;

                serviceNode = new everydesk.base.system.File({
                    name: path,
                    displayName: name,
                    path: '/fs/' + path + '/',
                    type: 'folder',
                    mimeType: 'folder',
                    created: null,
                    modified: null,
                    sourceType: service.type,
                    properties: {},
                    children: []
                });
            }
            else if (service.type.name === 'dropbox') {
                serviceNode = new everydesk.base.system.File({
                    name: path,
                    displayName: name,
                    path: '/fs/' + path + '/',
                    type: 'folder',
                    mimeType: 'folder',
                    created: null,
                    modified: null,
                    sourceType: service.type,
                    children: []
                });
            }
            me.__fileTree.children.push(serviceNode);
            me.__fsStates[path].loadCounter = 1;
            me.__connectors[path].ls(serviceNode.getPath(), me.__addFiles, me, path);
        },

        /**
         * Callback adding the files to the local filetree.
         */
        __addFiles: function(parent, files, name) {
            var me = this;
            if (!parent) {
                //me.__state.pop();
                me.__fsStates[name].loadCounter = 0;
                me.__fsStates[name].state = 'idle';
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'file',
                        name: name,
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                me.doActionQueue();
                return;
            }
            var pNode = me.__findParent(parent, me.__fileTree);
            pNode.setChildren(pNode.getChildren().concat(files));
            for (var i = 0; i < files.length; i++) {
                files[i].parent = pNode;
                if (files[i].getType() === 'folder') {
                    me.__fsStates[name].loadCounter++;
                    me.__connectors[name].ls(files[i].getPath(), me.__addFiles, me, name);
                }
            }
            me.__fsStates[name].loadCounter--;
            if (me.__fsStates[name].loadCounter === 0) {
                me.__fsStates[name].state = 'idle';
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'file',
                        name: name,
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                me.doActionQueue();
            }
        },

        /**
         * Util to find the parent node.
         */
        __findParent: function(path, node) {
            if (path === '/fs/') {
                return node;
            }
            var parts = path.split('/');
            var children = node.children;
            var ndx = 2;
            for (var i = 0; i < children.length; i++) {
                var nodeParts = children[i].getPath().split('/');
                if (parts[ndx] === nodeParts[ndx] && ndx == parts.length - 2) {
                    return children[i];
                }
                else if (parts[ndx] === nodeParts[ndx]) {
                    var current = children[i];
                    while (current && ndx < parts.length) {
                        ndx++;
                        var currChildren = current.getChildren();
                        for (var j = 0; j < currChildren.length; j++) {
                            var childParts = currChildren[j].getPath().split('/');
                            if (childParts[ndx] === decodeURIComponent(parts[ndx]) &&
                                ndx === parts.length - 2) {
                                return currChildren[j];
                            }
                            else if (childParts[ndx] === decodeURIComponent(parts[ndx])) {
                                current = currChildren[j];
                                break;
                            }
                        }
                    }
                }
            }
        },

        /**
         * Get the file for a specified path.
         */
        getFile: function(path) {
            var parts = path.split('/');
            var node = this.__fileTree;
            if (path === '/fs/') {
                return node;
            }

            var children = node.children;
            var ndx = 2;
            for (var i = 0; i < children.length; i++) {
                var nodeParts = children[i].getPath().split('/');
                if (parts[ndx] === nodeParts[ndx] && ndx === parts.length - 2) {
                    return children[i];
                }
                else if (children[i].getName() === decodeURIComponent(parts[ndx])) {
                    var current = children[i];
                    while (current && ndx < parts.length) {
                        var currChildren = current.getChildren();
                        ndx++;
                        for (var j = 0; j < currChildren.length; j++) {
                            var childParts = currChildren[j].getPath().split('/');
                            if (childParts[ndx] === parts[ndx] &&
                                (ndx === parts.length - 1 ||
                                ndx === parts.length - 2 && parts[parts.length - 1] === '')) {
                                return currChildren[j];
                            }
                            else if (childParts[ndx] === parts[ndx]) {
                                current = currChildren[j];
                                break;
                            }
                        }
                    }
                }
            }
        },

        open: function(files, callback, scope) {
            var me = this;
            var fs;
            if (files instanceof Array) {
                fs = files[0].parent ? files[0].parent : files[0];
                while (fs.parent) {
                    fs = fs.parent;
                }
                if (me.__fsStates[fs.getName()].state !== 'idle') {
                    me.__actionQueue.push({
                        action: 'open',
                        data: {
                            files: files,
                            callback: callback
                        }
                    });
                    return false;
                }
                me.__fsStates[fs.getName()].getCounter = 0;
                for (var i = 0; i < files.length; i++) {
                    me.__fsStates[fs.getName()].getCounter++;
                    me.__connectors[fs.getName()].get(files[i], callback, scope);
                }
            }
            else {
                fs = files.parent ? files.parent : files;
                while (fs.parent) {
                    fs = fs.parent;
                }
                if (me.__fsStates[fs.getName()].state !== 'idle') {
                    me.__actionQueue.push({
                        action: 'open',
                        data: {
                            files: files,
                            callback: callback
                        }
                    });
                    return false;
                }
                me.__fsStates[fs.getName()].getCounter = 0;
                me.__connectors[fs.getName()].get(files, callback, scope);
            }
            return true;
        },

        get: function(parentFolder, files, msgIndicator) {
            var me = this;
            var fs;
            if (files instanceof Array) {
                fs = files[0].parent ? files[0].parent : files[0];
                while (fs.parent) {
                    fs = fs.parent;
                }
                if (!msgIndicator) {
                    qx.event.message.Bus.dispatch(new qx.event.message.Message(
                        'actionstart', {
                            type: 'file',
                            name: fs.getName(),
                            icon: 'everydesk/icons/16x16/folder.png'
                        }
                    ));
                }
                if (me.__fsStates[fs.getName()].state !== 'idle') {
                    me.__actionQueue.push({
                        action: 'get',
                        data: {
                            files: files,
                            parentFolder: parentFolder
                        }
                    });
                    return false;
                }
                me.__fsStates[fs.getName()].state = 'busy';
                me.__fsStates[fs.getName()].getCounter = 0;
                for (var i = 0; i < files.length; i++) {
                    me.__fsStates[fs.getName()].getCounter++;
                    me.__connectors[fs.getName()].get(files[i], me.__getCallback, me);
                }
            }
            else {
                fs = files.parent ? files.parent : files;
                while (fs.parent) {
                    fs = fs.parent;
                }
                if (!msgIndicator) {
                    qx.event.message.Bus.dispatch(new qx.event.message.Message(
                        'actionstart', {
                            type: 'file',
                            name: fs.getName(),
                            icon: 'everydesk/icons/16x16/folder.png'
                        }
                    ));
                }
                if (me.__fsStates[fs.getName()].state !== 'idle') {
                    me.__actionQueue.push({
                        action: 'get',
                        data: {
                            files: files,
                            parentFolder: parentFolder
                        }
                    });
                    return false;
                }
                me.__fsStates[fs.getName()].getCounter = 0;
                me.__fsStates[fs.getName()].state = 'busy';
                me.__connectors[fs.getName()].get(files, me.__getCallback, me);
            }
            return true;
        },

        __getCallback: function(success, file, content) {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'file:get', {
                    success: success,
                    file: file,
                    content: content
                }
            ));
            var fs = file.parent ? file.parent : file;
            while (fs.parent) {
                fs = fs.parent;
            }
            me.__fsStates[fs.getName()].getCounter--;
            if (me.__fsStates[fs.getName()].getCounter === 0) {
                me.__fsStates[fs.getName()].state = 'idle';
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'file',
                        name: fs.getName(),
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                return;
            }
            me.__fsStates[fs.getName()].state = 'idle';
            me.doActionQueue();
        },

        save: function(parentFolder, name, content) {
            var me = this;
            var fs = parentFolder.parent ? parentFolder.parent : parentFolder;
            while (fs.parent) {
                fs = fs.parent;
            }
            me.__connectors[fs.getName()].save(parentFolder, name, content, me.__saveCallback, me);
            return true;
        },

        __saveStart: function(evt, msgIndicator) {
            var me = this;
            var parentFolder = evt.getData().file;
            var fs = parentFolder.parent ? parentFolder.parent : parentFolder;
            while (fs.parent) {
                fs = fs.parent;
            }
            if (!msgIndicator) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'file',
                        name: fs.getName(),
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
            }
            me.__fsStates[fs.getName()].state = 'busy';
        },

        __saveCallback: function(success, file, name) {
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'file:__save', {
                    success: success,
                    file: file,
                    name: name
                }
            ));
        },

        __saveCompleted: function(evt) {
            var me = this;
            var file = evt.getData().file;
            var success = evt.getData().success;
            var fs = file.parent ? file.parent : file;
            while (fs.parent) {
                fs = fs.parent;
            }
            if (!success) {
                me.__fsStates[fs.getName()].state = 'idle';
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'file',
                        name: fs.getName(),
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                return;
            }
            me.__fsStates[fs.getName()].state = 'idle';
            if (me.doActionQueue()) {
                me.refresh(file, fs.getName());
            }
        },

        cp: function(source, destination, msgIndicator) {
            var me = this;
            var fs = source.parent ? source.parent : source;
            while (fs.parent) {
                fs = fs.parent;
            }
            if (me.__fsStates[fs.getName()].state !== 'idle') {
                me.__actionQueue.push({
                    action: 'cp',
                    data: {
                        source: source,
                        destination: destination
                    }
                });
                return false;
            }
            if (!msgIndicator) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'file',
                        name: fs.getName(),
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
            }
            me.__fsStates[fs.getName()].state = 'busy';
            me.__connectors[fs.getName()].cp(source, destination, me.__mvCallback, me);
        },

        mkdir: function(parentFolder, name, msgIndicator) {
            var me = this;
            var fs = parentFolder.parent ? parentFolder.parent : parentFolder;
            while (fs.parent) {
                fs = fs.parent;
            }
            if (me.__fsStates[fs.getName()].state !== 'idle') {
                me.__actionQueue.push({
                    action: 'mkdir',
                    data: {
                        parent: parentFolder,
                        name: name
                    }
                });
                return false;
            }
            if (!msgIndicator) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'file',
                        name: fs.getName(),
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
            }
            me.__fsStates[fs.getName()].state = 'busy';
            me.__connectors[fs.getName()].mkdir(parentFolder, name, me.__mkdirCallback, me);
            return true;
        },

        __mkdirCallback: function(success, file) {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'file:mkdir', {
                    success: success,
                    file: file
                }
            ));
            var fs = file.parent ? file.parent : file;
            while (fs.parent) {
                fs = fs.parent;
            }
            if (!success) {
                me.__fsStates[fs.getName()].state = 'idle';
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'file',
                        name: fs.getName(),
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
                return;
            }
            me.__fsStates[fs.getName()].state = 'idle';
            if (me.doActionQueue()) {
                me.refresh(file, fs.getName());
            }
        },

        rm: function(files, msgIndicator) {
            var me = this;
            var fs;
            if (files instanceof Array) {
                fs = files[0].parent ? files[0].parent : files[0];
                while (fs.parent) {
                    fs = fs.parent;
                }
                if (!msgIndicator) {
                    qx.event.message.Bus.dispatch(new qx.event.message.Message(
                        'actionstart', {
                            type: 'file',
                            name: fs.getName(),
                            icon: 'everydesk/icons/16x16/folder.png'
                        }
                    ));
                }
                if (me.__fsStates[fs.getName()].state !== 'idle') {
                    me.__actionQueue.push({
                        action: 'rm',
                        data: {
                            files: files
                        }
                    });
                    return false;
                }
                me.__fsStates[fs.getName()].state = 'busy';
                me.__fsStates[fs.getName()].removeCounter = 0;
                for (var i = 0; i < files.length; i++) {
                    me.__fsStates[fs.getName()].removeCounter++;
                    me.__connectors[fs.getName()].rm(files[i], me.__rmCallback, me);
                }
            }
            else {
                fs = files.parent ? files.parent : files;
                while (fs.parent) {
                    fs = fs.parent;
                }
                if (!msgIndicator) {
                    qx.event.message.Bus.dispatch(new qx.event.message.Message(
                        'actionstart', {
                            type: 'file',
                            name: fs.getName(),
                            icon: 'everydesk/icons/16x16/folder.png'
                        }
                    ));
                }
                if (me.__fsStates[fs.getName()].state !== 'idle') {
                    me.__actionQueue.push({
                        action: 'rm',
                        data: {
                            files: files
                        }
                    });
                    return false;
                }
                me.__fsStates[fs.getName()].state = 'busy';
                me.__fsStates[fs.getName()].removeCounter = 1;
                // me.__davFS.rm(files, me.__rmCallback, me);
                me.__connectors[fs.getName()].rm(files, me.__rmCallback, me);
            }
            return true;
        },

        __rmCallback: function(success, file) {
            var me = this;
            var fs = file.parent ? file.parent : file;
            file = fs;
            while (fs.parent) {
                fs = fs.parent;
            }
            me.__fsStates[fs.getName()].removeCounter--;
            // Event dispatches only the last succes status! Fix this!
            if (me.__fsStates[fs.getName()].removeCounter === 0) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'file:rm', {
                        success: success
                    }
                ));
                me.__fsStates[fs.getName()].state = 'idle';
                if (me.doActionQueue()) {
                    me.refresh(file, fs.getName());
                }
            }
        },

        mv: function(oldFile, newPath, msgIndicator) {
            var me = this;
            var fs = oldFile.parent ? oldFile.parent : oldFile;
            while (fs.parent) {
                fs = fs.parent;
            }
            if (me.__fsStates[fs.getName()].state !== 'idle') {
                me.__actionQueue.push({
                    action: 'mv',
                    data: {
                        old: oldFile,
                        path: newPath
                    }
                });
                return false;
            }
            if (!msgIndicator) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'file',
                        name: fs.getName(),
                        icon: 'everydesk/icons/16x16/folder.png'
                    }
                ));
            }
            me.__fsStates[fs.getName()].state = 'busy';
            // newPath = newPath.replace('/fs/' + fs.getName() + '/', '');
            me.__connectors[fs.getName()].mv(oldFile, newPath, me.__mvCallback, me);
        },

        __mvCallback: function(success, newFile, oldFile) {
            var me = this;
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'file:mv', {
                    success: success,
                    file: oldFile
                }
            ));
            var fs = oldFile.parent ? oldFile.parent : oldFile;

            while (fs.parent) {
                fs = fs.parent;
            }
            var newNode = me.__findParent(newFile, me.__fileTree);
            me.__fsStates[fs.getName()].state = 'idle';
            if (me.doActionQueue()) {
                me.refresh(oldFile.parent, fs.getName());
            }
            if (me.doActionQueue()) {
                me.refresh(newNode, fs.getName());
            }
        },

        load: function() {
            var me = this;
            me.__loadActive = 1;
            me.__fileTree = {
                name: 'Files',
                path: '/fs/',
                properties: {
                    type: 'folder'
                },
                children: []
            };
            me.__getFileSystems();
        },

        refresh: function(folder, name) {
            var me = this;
            if (folder instanceof String) {
                if (folder === '/fs/') {
                    this.__load();
                }
                var parts = folder.split('/');
                var fs = parts[2];
                me.__fsStates[fs].loadCounter = 1;
                me.__fsStates[fs].state = 'busy';
                me.__connectors[fs].ls(folder, me.__addFiles, me, fs);
            }
            var file = this.getFile(folder.getPath()); // .children.length = 0;
            file.setChildren([]);
            if (folder.getPath() === '/fs/') {
                this.__load();
            }
            else {
                me.__fsStates[name].loadCounter = 1;
                me.__fsStates[name].state = 'busy';
                me.__connectors[name].ls(folder.getPath(), me.__addFiles, me, name);
            }
        },

        getFileSystem: function() {
            return this.__fileTree;
        },

        doActionQueue: function() {
            var me = this;
            if (me.__actionQueue.length === 0) {
                return true;
            }
            var action = me.__actionQueue.pop();
            if (action.action === 'rm') {
                me.rm(action.data.files, true);
            }
            else if (action.action === 'get') {
                me.mkdir(action.data.parent, action.data.name, true);
            }
            else if (action.action === 'mkdir') {
                me.mkdir(action.data.parent, action.data.name, true);
            }
            else if (action.action === 'mv') {
                me.mv(action.data.old, action.data.path, true);
            }
            else if (action.action === 'save') {
                me.mv(action.data.parent, action.data.name, action.data.content, true);
            }
            return false;
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
    },

    construct: function() {
        var me = this;
        me.base(arguments);
        /*
        me.__davFS = new everydesk.base.filesystem.Webdav();
        me.__davFS.setUrl(window.location.origin);
        me.__davFS.setFileSystemPath('/fs');
        me.__dropbox = new everydesk.base.filesystem.Dropbox();
        me.__dropbox.setFileSystemPath('/fs');
        */
        qx.event.message.Bus.subscribe('file:__savecompleted', me.__saveCompleted, me);
        qx.event.message.Bus.subscribe('file:__savestarted', me.__saveStart, me);
    }
});
