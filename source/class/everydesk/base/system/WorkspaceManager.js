/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.WorkspaceManager', {
    extend: qx.core.Object,
    type: 'singleton',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {

        /*
        * {
        *   $WORKSPACENAME: {
        *       active: '',
        *       states: [{
        *           plugin: '',
        *           state: {}
        *       }]
        *   },
        *
        * }
        */
        __workspaces: null,

        __current: 'default',

        loadWorkspaces: function() {
            var me = this;
            var request = new qx.io.request.Xhr('/workspace', 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.__workspaces = evt.getTarget().getResponse();
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'workspaceloaded',
                    me.__workspaces));
            });
            request.send();
        },

        saveWorkspaces: function() {
        },

        addWorkspace: function(workspace) {
            return workspace;
        },

        removeWorkspace: function(name) {
            return name;
        },

        getCurrentWorkspace: function() {
            return this.__current;
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        /**
         * Fired when the login was successful.
         */
        loadsuccess: 'qx.event.type.Data',
        /**
         * Fired when the login was not successful.
         */
        loadfailure: 'qx.event.type.Event'
    }
});
