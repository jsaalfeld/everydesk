/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.TaskManager', {
    extend: qx.core.Object,
    type: 'singleton',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        tasks: new qx.type.Array(),
        activeTask: null,
        idSeed: 0,

        __genId: function() {
            var me = this;
            return (++me.idSeed);
        },

        getTask: function(id) {
            var me = this;
            for (var i = 0; i < me.tasks.length; i++) {
                if (me.tasks[i].id === id) {
                    return me.tasks[i];
                }
            }
        },

        getActiveTask: function() {
            var me = this;
            return me.activeTask;
        },

        start: function(evt) {
            var me = this;
            var task =
                new everydesk.base.system.Task(me.__genId(), evt.getData());
            me.tasks.push(task);
            if (task.getType() === 'app') {
                var msg = new qx.event.message.Message('tasknew', task);
                msg.setSender({
                    source: 'taskmanager'
                });
                qx.event.message.Bus.dispatch(msg);
            }
        },

        close: function(evt) {
            var me = this;
            var task = evt.getData();
            if (task.getType() === 'app') {
                var msg = new qx.event.message.Message('taskremoved', task.getId());
                msg.setSender({
                    source: 'taskmanager'
                });
                qx.event.message.Bus.dispatch(msg);
            }
            me.tasks.remove(task);
        },

        __activate: function(msg) {
            var me = this;
            var sender = msg.getSender();
            var task = msg.getData();
            me.activate(task, sender.source);
        },

        activate: function(task, cause) {
            var me = this;
            me.activeTask = task;
            var msg = new qx.event.message.Message('taskactivate', task);
            msg.setSender({
                source: 'taskmanager',
                cause: cause
            });
            qx.event.message.Bus.dispatch(msg);
        },

        __deactivate: function(msg) {
            var me = this;
            var task = msg.getData();
            var sender = msg.getSender();
            me.deactivate(task, sender.source);
        },

        deactivate: function(task, cause) {
            var me = this;
            me.activeTask = null;
            var msg = new qx.event.message.Message('taskdeactivate', task);
            msg.setSender({
                source: 'taskmanager',
                cause: cause
            });
            qx.event.message.Bus.dispatch(msg);
        },

        __openFile: function(msg) {
            var starter = everydesk.base.system.StarterManager.getInstance();
            var appName = '';
            if (msg.getData().getMimeType() === 'image/jpeg') {
                appName = 'ImageViewer';
            }
            else if (msg.getData().getMimeType() ===
                'application/vnd.oasis.opendocument.text') {
                appName = 'Office';
            }
            else if (msg.getData().getMimeType() ===
                'application/vnd.oasis.opendocument.spreadsheet') {
                appName = 'Office';
            }
            else if (msg.getData().getMimeType() ===
                'application/vnd.oasis.opendocument.presentation') {
                appName = 'Office';
            }
            else if (msg.getData().getMimeType() ===
                'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
                appName = 'Office';
            }
            else if (msg.getData().getMimeType() ===
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                appName = 'Office';
            }
            else if (msg.getData().getMimeType() ===
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                appName = 'Office';
            }
            var app;
            for (var i = 0; i < starter.apps.length; i++) {
                if (starter.apps[i].name === appName) {
                    app = starter.apps[i];
                    break;
                }
            }
            app.file = msg.getData();
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'appstart', app));
        },

        __showDialog: function(msg) {
            qx.event.message.Bus.dispatch(new qx.event.message.Message(
                'appstart', {
                    type: 'dialog',
                    endpoint: msg.getData().endpoint,
                    icon: msg.getData().icon,
                    name: msg.getData().name,
                    file: msg.getData().file,
                    callback: msg.getData().callback,
                    app: msg.getSender()
                }
            ));
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        newtask: 'qx.event.type.Event',
        removedtask: 'qx.event.type.Event'
    },

    /*
    ***************************************************************************
     FUNCTIONS
    ***************************************************************************
    */

    /**
    * Create a new login screen.
    */
    construct: function() {
        var me = this;
        qx.event.message.Bus.subscribe('appstart', me.start, me);
        qx.event.message.Bus.subscribe('appdeactivate', me.__deactivate, me);
        qx.event.message.Bus.subscribe('appactivate', me.__activate, me);
        qx.event.message.Bus.subscribe('appclose', me.close, me);
        qx.event.message.Bus.subscribe('openfile', me.__openFile, me);
        qx.event.message.Bus.subscribe('showdialog', me.__showDialog, me);
        me.base(arguments);
    }
});
