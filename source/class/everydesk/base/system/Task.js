/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.Task', {
    extend: qx.core.Object,

    /*
    ***************************************************************************
     PROPERTIES
    ***************************************************************************
    */
    properties: {
    },

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __id: null,
        __active: false,
        __minimized: false,
        __desktopWidget: false,
        __ui: null,
        __endpoint: '',
        __type: '',
        __name: '',
        __icon: '',
        __workspace: '',
        __referer: null,

        getType: function() {
            return this.__type;
        },

        getIcon: function() {
            return this.__icon;
        },

        getName: function() {
            return this.__name;
        },

        getId: function() {
            return this.__id;
        },

        setActive: function(value) {
            var me = this;
            me.__ui.setActive(value);
        },

        getUi: function() {
            return this.__ui;
        },

        close: function() {
            var me = this;
            me.__ui.close();
            me.__taskClosed();
        },

        __taskClosed: function() {
            var me = this;
            qx.event.message.Bus.dispatch(
                new qx.event.message.Message('beforeappclose', me));

            // TODO: Stuff to do ui things.
            qx.event.message.Bus.dispatch(
                new qx.event.message.Message('appclose', me));

            // TODO: Stuff to close the task.
            qx.event.message.Bus.dispatch(
                new qx.event.message.Message('afterappclose', me));
        },

        __taskActiviated: function(evt) {
            var me = this;
            var msg;
            if (evt.getCurrentTarget().isActive()) {
                msg = new qx.event.message.Message('appactivate', me);
            }
            else {
                msg = new qx.event.message.Message('appdeactivate', me);
            }
            msg.setSender({source: 'task'});
            qx.event.message.Bus.dispatch(msg);
            return;
        },

        __activateTask: function(evt) {
            var me = this;
            if (me.__id === evt.getData().getId()) {
                if (!me.__ui.isVisible()) {
                    me.__ui.show();
                }
                me.__ui.setActive(true);
            }
        },

        __deactivateTask: function(msg) {
            var me = this;
            if (me.__id === msg.getData().getId()) {
                if (msg.getSender().cause === 'mainbar') {
                    me.__ui.minimize();
                }
            }
        },

        start: function() {
            var me = this;
            me.debug('Starting Task');
            qx.event.message.Bus.dispatch(
                new qx.event.message.Message('beforetaskstart', me));

            var clazz = qx.Class.getByName(me.__endpoint);
            if (!clazz) {
                // show error
                return;
            }
            var app = new clazz();
            me.__ui = app;
            qx.event.message.Bus.dispatch(
                new qx.event.message.Message('taskstart', me));
            me.__ui.addListener('close', me.__taskClosed, me);
            if (me.__type === 'app') {
                me.__ui.addListener('changeActive', me.__taskActiviated, me);
            }
            // TODO: Stuff to start the task.
            qx.event.message.Bus.dispatch(
                new qx.event.message.Message('aftertaskstart', me));
            me.__ui.open(me.__file);
            if (me.__type === 'dialog') {
                me.__ui.setReferer(me.__referer);
                me.__ui.setCaption(me.__name);
                me.__ui.setCallback(me.__callback);
            }
        },

        startPlugin: function(state) {
            var me = this;
            if (state[0] === 'complete') {
                me.debug('Plugin loaded');
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionend', {
                        type: 'plugin',
                        icon: 'everydesk/icons/16x16/application.png'
                    }
                ));
                this.start();
            }
            else {
                // TODO show error
            }
        },

        __findEndpoint: function(endpoint) {
            var parts = qx.io.PartLoader.getInstance().getParts();
            // strip endpoint class
            var ndx = endpoint.lastIndexOf('.');
            endpoint = endpoint.substring(0, ndx);
            for (var key in parts) {
                if (key.indexOf(endpoint) >= 0) {
                    return key;
                }
            }
            return endpoint;
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        beforeStart: 'qx.event.type.Event',
        beforeClose: 'qx.event.type.Event',
        start: 'qx.event.type.Event',
        close: 'qx.event.type.Event',
        afterStart: 'qx.event.type.Event',
        afterClose: 'qx.event.type.Event',
        construct: 'qx.event.type.Event',
        activate: 'qx.event.type.Event',
        minimized: 'qx.event.type.Event'
    },

    /*
    ***************************************************************************
     FUNCTIONS
    ***************************************************************************
    */

    /**
    */
    construct: function(id, data) {
        var me = this;
        me.debug('Creating Task');
        me.base(arguments);
        me.__id = id;

        me.__type = data.type;
        me.__endpoint = data.endpoint;
        me.__icon = data.icon;
        me.__name = data.name;
        me.__file = data.file;
        if (me.__type === 'dialog') {
            me.__referer = data.app;
            me.__callback = data.callback;
        }
        me.__workspace = everydesk.base.system.WorkspaceManager.getInstance()
            .getCurrentWorkspace();
        if (me.__type === 'app' || me.__type === 'dialog') {
            if (!qx.Class.isDefined(me.__endpoint)) {
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'actionstart', {
                        type: 'plugin',
                        icon: 'everydesk/icons/16x16/application.png'
                    }
                ));
                var endpoint = me.__findEndpoint(me.__endpoint);
                qx.io.PartLoader.require([endpoint], me.startPlugin, me);
            }
            else {
                me.start();
            }
        }
        qx.event.message.Bus.subscribe('taskactivate', me.__activateTask, me);
        qx.event.message.Bus.subscribe('taskdeactivate', me.__deactivateTask, me);
    }
});
