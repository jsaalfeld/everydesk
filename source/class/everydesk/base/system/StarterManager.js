/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.system.StarterManager', {
    extend: qx.core.Object,
    type: 'singleton',

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        apps: null,
        __loadCounter: 0,
        __maxLoads: 2,

        loadApps: function() {
            var me = this;
            var request = new everydesk.base.io.Xhr('/starter', 'GET');
            request.setRequestHeader(
                'X-EVERYDESK-SESSION',
                everydesk.base.system.Session.SESSIONKEY);
            request.addListener('success', function(evt) {
                me.apps = evt.getTarget().getResponse();
                console.log(me.apps);
                for (var i = 0; i < me.apps.length; i++) {
                    console.log(me.apps[i]);
                    me.apps[i].type = me.apps[i].type.name;
                }
                qx.event.message.Bus.dispatch(new qx.event.message.Message(
                    'loadsuccessStarter',
                    me.apps));
            });
            request.addListener('fail', function() {
                if (me.__loadCounter < me.__maxLoads) {
                    me.__loadCounter++;
                    request.send();
                }
            });
            me.__loadCounter++;
            request.send();
        },

        getApps: function() {
            return this.apps;
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        /**
         * Fired when the login was successful.
         */
        loadsuccess: 'qx.event.type.Data',
        /**
         * Fired when the login was not successful.
         */
        loadfailure: 'qx.event.type.Event'
    }
});
