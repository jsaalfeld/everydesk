/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
qx.Class.define('everydesk.base.io.Xhr', {
    extend: qx.io.request.Xhr,

    members: {

        __dialog: null,
        __refreshContainer: null,

        setResponseType: function(type) {
            this._transport.getRequest().responseType = type;
        },
      
        _getConfiguredRequestHeaders: function() {
            var headers = {},
                isAllowsBody = qx.util.Request.methodAllowsRequestBody(this.getMethod());
      
            // Include Cache-Control header if configured
            if (qx.lang.Type.isString(this.getCache())) {
                headers['Cache-Control'] = this.getCache();
            }
      
            // By default, set content-type urlencoded for requests with body
            if (this.getRequestData() !== 'null' && isAllowsBody) {
                headers['Content-Type'] = 'application/x-www-form-urlencoded';
            }
      
            // What representations to accept
            if (this.getAccept()) {
                if (qx.core.Environment.get('qx.debug.io')) {
                    this.debug('Accepting: "' + this.getAccept() + '"');
                }
                headers['Accept'] = this.getAccept();
            }
      
            return headers;
        },

        _onReadyStateChange: function() {
            this.base(arguments);
            if (this.isDone()) {
                if (this.getStatus() === 401) {
                    this.__showSessionDialog();
                }
            }
        },

        __showSessionDialog: function() {
            console.log('restore session');
            var me = this;
            this.__dialog = new qx.ui.window.Window('Restore Session');
            this.__dialog.setLayout(new qx.ui.layout.VBox(5));
            this.__dialog.add(new qx.ui.basic.Label('Your Session has expired.'));
            this.__dialog.add(new qx.ui.basic.Label('You can refresh the Session or reload everydesk.'));
            var buttonContainer = new qx.ui.container.Composite();
            var buttonLayout = new qx.ui.layout.HBox(0, 'right');
            buttonContainer.setLayout(buttonLayout);
            var refresh = new qx.ui.form.Button('Refresh');
            refresh.setMargin(5, 3, 5, 3);
            refresh.addListener('execute', this.__refreshSession, this);
            var reload = new qx.ui.form.Button('Reload');
            reload.setMargin(5, 3, 5, 3);
            reload.addListener('execute', this.__reload, this);
            buttonContainer.add(refresh);
            buttonContainer.add(reload);
            this.__dialog.add(buttonContainer);
            this.__refreshContainer = new qx.ui.container.Composite();
            var refreshLayout = new qx.ui.layout.HBox(0);
            this.__refreshContainer.setLayout(refreshLayout);
            this.__refreshPassword = new qx.ui.form.PasswordField();
            this.__refreshPassword.setMargin(5, 3, 5, 3);
            this.__refreshPassword.addListener('keypress', function(data) {
                if (data.getKeyCode() === 13) {
                    me.__doRefresh();
                }
            }, this);
            var doRefresh = new qx.ui.form.Button('Go');
            doRefresh.setMargin(5, 3, 5, 3);
            doRefresh.addListener('execute', this.__doRefresh, this);
            this.__refreshContainer.add(this.__refreshPassword, {
                flex: 1
            });
            this.__refreshContainer.add(doRefresh);
            this.__refreshContainer.hide();
            this.__dialog.open();
            var width = window.innerWidth;
            var appWidth = this.__dialog.getWidth();
            var height = window.innerHeight;
            var appHeight = this.__dialog.getHeight();
            var top = height - appHeight;
            if (top > 0) {
                top = Math.floor(top/2);
            }
            else {
                top = 0;
            }
            var left = width - appWidth;
            if (left > 0) {
                left = Math.floor(left/2);
            }
            else {
                left = 0;
            }
            this.__dialog.moveTo(left, top);
        },

        __refreshSession: function() {
            this.__dialog.add(this.__refreshContainer);
            this.__refreshContainer.show();
        },

        __doRefresh: function() {
            var me = this;
            var password = this.__refreshPassword.getValue();
            var request = new qx.io.request.Xhr('/login', 'POST');
            request.setRequestData({
                user: 'admin',//everydesk.base.system.Session.USERNAME,
                password: password
            });
            request.addListener('success', function(evt) {
                console.log(evt);
                var response = evt.getTarget().getResponse();
                everydesk.base.system.Session.SESSIONKEY = response.key;
                me.__dialog.close();
                me.__dialog.destroy();
            });
            request.addListener('statusError', function(evt) {
                var errMsg = new qx.ui.basic.Label('Wrong password!');
                errMsg.setMargin(0, 0, 0, 15);
                errMsg.setTextColor('red');
                me.__dialog.add(errMsg);
            });
            request.send();
        },

        __reload: function() {
            window.location.reload();
        }
    }
});
