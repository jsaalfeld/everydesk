/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/background/*)
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.widget.ActionBar', {
    extend: qx.ui.container.Composite,

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __actionFieldHidden: null,
        __actionIndicator: null,
        __actionIcons: [],
        __actionField: null,

        __startAction: function(evt) {
            var me = this;
            if (me.__actionFieldHidden) {
                me.__showActionField(evt.getData().icon);
                me.__actionFieldHidden = false;
            }
            else {
                me.__addActionIcon(evt.getData().icon);
            }
            me.__actionIndicator.setVisibility('visible');
        },

        __endAction: function(evt) {
            var me = this;
            me.__removeActionIcon(evt.getData().icon);
            if (!me.__actionFieldHidden &&
                me.__actionIcons.length === 0) {
                me.__hideActionField();
                me.__actionFieldHidden = true;
            }
        },

        __addActionIcon: function(icon) {
            var me = this;
            var actionIcon = new qx.ui.basic.Image(icon);
            actionIcon.setHeight(18);
            actionIcon.setWidth(18);
            actionIcon.setScale(true);
            me.__actionIcons.push(actionIcon);
            for (var i = 0; i < me.__actionIcons.length; i++) {
                var row;
                var column;
                if (i < 3) {
                    row = 1;
                    column = 3 - i;
                }
                else {
                    row = 0;
                    column = 6 - i;
                }
                me.__iconContainer.add(me.__actionIcons[i], {
                    row: row,
                    column: column
                });
            }
        },

        __removeActionIcon: function(icon) {
            var me = this;
            for (var i = 0; i < me.__actionIcons.length; i++) {
                if (me.__actionIcons[i].getSource() === icon) {
                    me.__iconContainer.remove(me.__actionIcons[i]);
                    me.__actionIcons.splice(i, 1);
                }
            }
            for (i = 0; i < me.__actionIcons.length; i++) {
                var row;
                var column;
                if (i < 3) {
                    row = 1;
                    column = 3 - i;
                }
                else {
                    row = 0;
                    column = 6 - i;
                }
                me.__iconContainer.add(me.__actionIcons[i], {
                    row: row,
                    column: column
                });
            }
        },

        __showActionField: function(icon) {
            var me = this;
            var animation = {
                duration: 300,
                timing: 'ease-in',
                keep: 100,
                keyFrames: {
                    0: {
                        rotate: ['90deg'],
                        skew: ['0deg']
                    },
                    100: {
                        rotate: ['0deg'],
                        skew: ['0deg']
                    }
                }

            };
            if (me.__actionField.getContentElement().getDomElement()) {
                var handle = qx.bom.element.Animation.animate(
                    me.__actionField.getContentElement().getDomElement(), animation);
                handle.on('end', function() {
                    me.__addActionIcon(icon);
                });
            }
        },

        __hideActionField: function() {
            var me = this;
            var animation = {
                duration: 800,
                timing: 'ease-out',
                keep: 100,
                keyFrames: {
                    0: {
                        rotate: ['0deg'],
                        skew: ['0deg']
                    },
                    50: {
                        rotate: ['0deg'],
                        skew: ['0deg']
                    },
                    100: {
                        rotate: ['90deg'],
                        skew: ['0deg']
                    }
                }

            };
            if (me.__actionField.getContentElement().getDomElement()) {
                var handle = qx.bom.element.Animation.animate(
                    me.__actionField.getContentElement().getDomElement(), animation);
                handle.on('end', function() {
                });
            }
            else {
                return;
            }
        }
    },

    /**
    * Create a new login screen.
    */
    construct: function() {
        var me = this;
        me.base(arguments);

        me.setLayout(new qx.ui.layout.Canvas());
        me.__actionField = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
        me.__actionField.set({
            decorator: 'actionField'
        });
        me.__actionField.setWidth(120);
        me.__actionField.setHeight(60);
        me.__actionField.setBackgroundColor('rgba(0, 0, 0, 0.5)');
        me.__actionIndicator = new qx.ui.basic.Image('everydesk/icons/16x16/loading2.gif');
        me.__actionIndicator.setWidth(100);
        me.__actionIndicator.setHeight(10);
        me.__actionIndicator.setScale(true);
        me.__actionField.add(me.__actionIndicator, {
            right: 8,
            bottom: 3
        });
        me.__actionFieldHidden = false;
        me.__iconContainer = new qx.ui.container.Composite();
        var contLayout = new qx.ui.layout.Grid();
        contLayout.setColumnWidth(0, 22);
        contLayout.setColumnWidth(1, 22);
        contLayout.setColumnWidth(2, 22);
        contLayout.setColumnWidth(3, 22);
        contLayout.setRowHeight(0, 22);
        contLayout.setRowHeight(1, 20);
        me.__iconContainer.setLayout(contLayout);
        me.__iconContainer.setWidth(66);
        me.__iconContainer.setHeight(44);
        me.add(me.__actionField, {
            bottom: 0,
            right: 0
        });
        me.add(me.__iconContainer, {
            bottom: 12,
            right: 12
        });
        qx.event.message.Bus.subscribe('actionstart', me.__startAction, me);
        qx.event.message.Bus.subscribe('actionend', me.__endAction, me);
    }
});
