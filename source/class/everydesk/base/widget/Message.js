/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/background/*)
 * @asset(everydesk/icons/16x16/*)
 * @asset(everydesk/icons/32x32/*)
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.widget.Message', {
    extend: qx.ui.window.Window,

    statics: {
        OK: 1,
        YES: 1,
        NO: -1,
        CANCEL: -1,
        Buttons: {
            OK: 1,
            CANCEL: 2,
            OK_CANCEL: 3,
            YES_NO: 4
        },
        Type: {
            ALERT: 1,
            QUESTION: 2,
            INFO: 3
        }
    },
    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        __options: null,

        init: function() {
            var me = this;
            me.debug('Initialize Message');
            me.setCaption(me.__options.title);
            var image = '';
            switch (me.__options.type) {
            case 1: image = 'everydesk/icons/32x32/warning.png'; break;
            case 2: image = 'everydesk/icons/32x32/question.png'; break;
            case 3: image = 'everydesk/icons/32x32/information.png'; break;
            default: image = '';
            }
            var btnText1 = '';
            var btnText2 = '';
            switch (me.__options.buttons) {
            case 1: btnText1 = 'Ok'; break;
            case 2: btnText1 = 'Cancel'; break;
            case 3: btnText1 = 'Ok'; btnText2 = 'Cancel'; break;
            case 4: btnText1 = 'Yes'; btnText2 = 'No'; break;
            default: break;
            }
            var mainContainer = new qx.ui.container.Composite();
            mainContainer.setPadding(0, 0, 0, 0);
            var contLayout = new qx.ui.layout.VBox();
            mainContainer.setLayout(contLayout);
            var img = new qx.ui.basic.Image(image);
            img.setMargin(5, 10, 5, 10);
            var label = new qx.ui.basic.Label(me.__options.text);
            label.setHeight(32);
            label.setRich(true);
            label.setWrap(true);
            label.setMargin(5, 5, 5, 10);
            var contentContainer = new qx.ui.container.Composite();
            var contentLayout = new qx.ui.layout.HBox();
            contentContainer.setLayout(contentLayout);
            contentContainer.add(img);
            contentContainer.add(label, {
                flex: 1
            });
            mainContainer.add(contentContainer, {
                flex: 1
            });
            var buttonContainer = new qx.ui.container.Composite();
            var buttonLayout = new qx.ui.layout.HBox(0, 'right');
            buttonContainer.setLayout(buttonLayout);
            var button1 = new qx.ui.form.Button(btnText1);
            button1.setMargin(5, 3, 5, 3);
            button1.addListener('execute', me.__button1Pressed, me);
            if (btnText2 !== '') {
                var button2 = new qx.ui.form.Button(btnText2);
                button2.setMargin(5, 3, 5, 3);
                button2.addListener('execute', me.__button2Pressed, me);
                buttonContainer.add(button2);
            }
            buttonContainer.add(button1);
            mainContainer.add(buttonContainer);
            var layout = new qx.ui.layout.VBox();
            me.setLayout(layout);
            me.add(mainContainer);
        },
        __button1Pressed: function() {
            this.__options.callback.call(this, 1);
            this.close();
        },

        __button2Pressed: function() {
            this.__options.callback.call(this, -1);
            this.close();
        }
    },

    construct: function(options) {
        var me = this;
        me.__options = options;
        me.base(arguments);
        me.setShowMinimize(false);
        me.setShowMaximize(false);
        me.setContentPadding(0, 0, 0, 0);
        me.setWidth(300);
        me.setHeight(100);
        me.init();
        me.center();
    }
});
