/* Copyright © 2016 Raimund Renkert <raimund@renkert.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @asset(everydesk/background/*)
 * @asset(everydesk/*)
 */
qx.Class.define('everydesk.base.widget.MainBar', {
    extend: qx.ui.container.Composite,

    /*
    ***************************************************************************
     PROPERTIES
    ***************************************************************************
    */
    properties: {
        clockInterval: {
            check: 'Number',
            init: 500
        }
    },

    /*
    ***************************************************************************
     MEMBERS
    ***************************************************************************
    */
    members: {
        startMenu: null,
        taskbar: null,
        tray: null,
        clockTimer: null,
        clock: null,
        toggleGroup: null,
        __actionIndicator: null,
        __actionField: null,
        __actionIcons: [],
        __actionFieldHidden: true,
        __isFullscreen: false,

        __tasks: {},

        __clickedStartMenu: function(evt) {
            var me = this;
            if (evt.isLeftPressed()) {
                me.fireEvent('mainmenu', qx.event.type.Event);
            }
        },

        startClock: function() {
            var me = this;
            var timer = me.clockTimer = qx.util.TimerManager.getInstance();
            timer.start(
                me.__updateClock,
                me.getClockInterval(),
                me,
                null,
                me.getClockInterval()
            );
        },

        __addTask: function(evt) {
            var me = this;
            var task = evt.getData();
            var button = new qx.ui.form.ToggleButton(
                task.getName(),
                'everydesk/icons/16x16/' + task.getIcon());
            button.setGroup(me.toggleGroup);
            button.set({
                appearance: 'taskbutton'
            });
            me.__tasks[task.getId()] = button;
            me.taskbar.add(button);
            button.addListener('execute', function() {
                var msg;
                if (button.getValue()) {
                    msg = new qx.event.message.Message('appactivate', task);
                }
                else {
                    msg = new qx.event.message.Message('appdeactivate', task);
                }
                msg.setSender({source: 'mainbar'});
                qx.event.message.Bus.dispatch(msg);
                return;
            });
            button.setValue(true);
        },

        __removeTask: function(evt) {
            var me = this;
            var task = evt.getData();
            var button = me.__tasks[task];
            me.taskbar.remove(button);
        },

        __activateTask: function(evt) {
            var me = this;
            var task = evt.getData();
            var button = me.__tasks[task.getId()];
            button.setValue(true);
        },

        __deactivateTask: function(evt) {
            var me = this;
            var task = evt.getData();
            var button = me.__tasks[task.getId()];
            button.setValue(false);
        },

        __updateClock: function() {
            var me = this;
            me.clock.setValue(
                new qx.util.format.DateFormat('dd.MM.y | HH:mm')
                    .format(new Date()));
        }
    },

    /*
    ***************************************************************************
     EVENTS
    ***************************************************************************
    */
    events: {
        mainmenu: 'qx.event.type.Event'
    },

    /**
    * Create a new login screen.
    */
    construct: function() {
        var me = this;
        me.base(arguments);

        me.toggleGroup = new qx.ui.form.RadioGroup();
        me.toggleGroup.setAllowEmptySelection(true);
        me.startMenu = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
        me.startMenu.setWidth(50);
        me.startMenu.setHeight(35);
        me.startMenu.set({
            decorator: 'taskbar'
        });
        me.startMenu.addListener('mouseup', me.__clickedStartMenu, me);
        me.startMenu.add(new qx.ui.basic.Image('everydesk/icons/32x32/everydesk3-32x32.png'), {
            left: 8
        });
        me.taskbar = new qx.ui.toolbar.ToolBar();
        me.taskbar.setHeight(35);
        me.taskbar.set({
            decorator: 'taskbar'
        });
        me.clockContainer= new qx.ui.container.Composite(new qx.ui.layout.Canvas());
        me.clockContainer.set({
            decorator: 'clock'
        });
        me.clockContainer.setWidth(120);
        me.clockContainer.setHeight(35);
        me.tray = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
        me.tray.set({
            decorator: 'tray'
        });
        me.tray.setWidth(50);
        me.tray.setHeight(35);
        me.tray.setBackgroundColor('rgba(0, 0, 0, 0.5)');
        me.clock = new qx.ui.basic.Label();
        me.clock.setTextAlign('center');
        me.clock.setWidth(110);
        me.clock.setHeight(14);
        me.clock.setFont(
            new qx.bom.Font(12, ['tahoma', 'arial', 'verdana', 'sans-serif']));
        me.clock.setTextColor('rgb(250, 250, 250)');
        me.fullScreen = new qx.ui.form.Button(
            null,
            'everydesk/icons/16x16/arrow_out.png');
        me.fullScreen.set({
            appearance: 'traybutton'
        });
        me.fullScreen.addListener('execute', function() {
            // ONLY FOR FIREFOX!
            // TODO: cross browser compatible
            if (me.__isFullscreen) {
                var rfs = window.document.exitFullscreen
                    || window.document.webkitExitFullscreen
                    || window.document.mozCancelFullScreen
                    || window.document.msExitFullscreen;
                rfs.call(window.document);
                me.fullScreen.setIcon('everydesk/icons/16x16/arrow_out.png');
                me.__isFullscreen = false;
            }
            else {
                var rfs = window.document.documentElement.requestFullscreen
                    || window.document.documentElement.webkitRequestFullscreen
                    || window.document.documentElement.mozRequestFullScreen
                    || window.document.documentElement.msRequestFullscreen;
                rfs.call(window.document.documentElement);
                me.fullScreen.setIcon('everydesk/icons/16x16/arrow_in.png');
                me.__isFullscreen = true;
            }
        }, me);
        window.document.documentElement.addEventListener('mozfullscreenchange', function() {
            console.log('changed');
        });
        me.tray.add(me.fullScreen, {
            right: 5,
            bottom: 3
        });
        me.clockContainer.add(me.clock, {
            right: 5,
            bottom: 8
        });

        me.setLayout(new qx.ui.layout.Canvas());
        me.add(me.startMenu, {
            left: 0,
            bottom: 0
        });
        me.add(me.taskbar, {
            bottom: 0,
            left: 50,
            right: 170
        });
        me.add(me.tray, {
            bottom: 0,
            right: 120
        });
        me.add(me.clockContainer, {
            bottom: 0,
            right: 0
        });
        me.startClock();
        qx.event.message.Bus.subscribe('tasknew', me.__addTask, me);
        qx.event.message.Bus.subscribe('taskremoved', me.__removeTask, me);
        qx.event.message.Bus.subscribe('taskactivate', me.__activateTask, me);
        qx.event.message.Bus.subscribe('taskdeactivate', me.__deactivateTask, me);
    }
});
